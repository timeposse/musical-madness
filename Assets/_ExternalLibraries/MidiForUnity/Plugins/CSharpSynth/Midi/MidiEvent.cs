﻿namespace CSharpSynth.Midi
{
    public class MidiEvent
    {
        //--Variables
        public uint deltaTime;
        public MidiHelper.MidiMetaEvent midiMetaEvent;
        public MidiHelper.MidiChannelEvent midiChannelEvent;
        public object[] Parameters;
        public byte parameter1;
        public byte parameter2;
        public byte channel;
        //--Public Methods
        public MidiEvent()
        {
            this.Parameters = new object[5];
            this.midiMetaEvent = MidiHelper.MidiMetaEvent.None;
            this.midiChannelEvent = MidiHelper.MidiChannelEvent.None;
        }
        public bool isMetaEvent()
        {
            return midiChannelEvent == MidiHelper.MidiChannelEvent.None;
        }
        public bool isChannelEvent()
        {
            return midiMetaEvent == MidiHelper.MidiMetaEvent.None;
        }
        public MidiHelper.ControllerType GetControllerType()
        {
            if (midiChannelEvent != MidiHelper.MidiChannelEvent.Controller)
                return MidiHelper.ControllerType.None;
            switch (parameter1)
            {
                case 1:
                    return MidiHelper.ControllerType.Modulation;
                case 7:
                    return MidiHelper.ControllerType.MainVolume;
                case 10:
                    return MidiHelper.ControllerType.Pan;
                case 64:
                    return MidiHelper.ControllerType.DamperPedal;
                case 121:
                    return MidiHelper.ControllerType.ResetControllers;
                case 123:
                    return MidiHelper.ControllerType.AllNotesOff;
                default:
                    return MidiHelper.ControllerType.Unknown;
            }
        }


        public string ToString(bool convertToGameFormat)
        {
            if (convertToGameFormat)
            {

                if (midiChannelEvent.ToString() == "Program_Change")
                {
                    string instrument = parameter1.ToString();
                    return "Program_Change," + channel.ToString() + "," + instrument + ",";
                }
                else if (midiChannelEvent.ToString() == "Pitch_Bend")
                {
                    return "Pitch_Bend," + channel.ToString() + "," + parameter1.ToString() + "," + parameter2.ToString();
                }
                else if (midiChannelEvent.ToString() == "Controller")
                {
                    return "Controller," + channel.ToString() + "," + parameter1.ToString() + "," + parameter2.ToString();
                }
                else if (midiChannelEvent.ToString() == "Note_On")
                {
                    return "Note_On," + channel.ToString() + "," + parameter1.ToString() + "," + parameter2.ToString();
                }
                else if (midiChannelEvent.ToString() == "Note_Off")
                {
                    return "Note_Off," + channel.ToString() + "," + parameter1.ToString() + ",";
                }
                else
                    return null;
                    
                
            }
            else
            {
                //midiEvent.channel, midiEvent.parameter1, midiEvent.parameter2, midiSequencer.currentPrograms[midiEvent.channel]
                return midiChannelEvent.ToString() + "," + midiMetaEvent.ToString() + "," + channel.ToString() + "," + parameter1.ToString() + "," + parameter2.ToString() + "," + deltaTime.ToString();
            }
            
        }
    }
}
