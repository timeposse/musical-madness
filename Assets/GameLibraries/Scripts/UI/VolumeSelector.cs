﻿using UnityEngine;
using System.Collections;

public class VolumeSelector : MonoBehaviour {
    UISlider slider;
    int volume = 0;
    public MidiController midiController;

	void Start () {
        slider = GetComponent<UISlider>();
        midiController = GameObject.Find("MidiController").GetComponent<MidiController>();
	}
	
	public void OnSliderChange () {
        if (slider != null)
        {
            int value = (int)(slider.sliderValue * 100);
            if (volume != value)
            {

                volume = value;

                Debug.Log("Volume is set to " + volume);
                if (midiController != null)
                    midiController.midiNoteVolume = volume;
                else
                    Debug.Log("Error: midiController not found");
            }
        }
        
        
       
	}
}
