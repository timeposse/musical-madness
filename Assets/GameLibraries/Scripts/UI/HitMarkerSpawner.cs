﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class HitMarkerSpawner : MonoBehaviour {
    public GameObject parent;
    public GameObject hitMarkerPrefab;
    GameObject lastSpawnedObj;
    public Transform[] spawnPoints;
    int activePoint = 0;
    public List<int> availableInstruments;
    public List<int> instrumentsToSpawn;

    public List<SpawnEvent> queue;
    public float combineNoteThreshold = 0.0625f;

    [System.Serializable]
    public class SpawnEvent
    {
        public string note;
        public int instrument;
        public float time;

        public SpawnEvent(int note, int instrument)
        {
            this.note = KeyNoteMap.ConvertKeyIndexToLetter(note - 37);

            this.instrument = instrument;
        }

    }


    void Start()
    {
        queue = new List<SpawnEvent>();
    }

    float lastSpawnTime = 0f;
    
    void Update()
    {
        if (queue.Count > 0)
        {
            if (lastSpawnedObj == null)//First note hasn't spawned yet
            {
                InstantiateHitMarker();
            }
            else //song has already started playing
            {
                //Debug.Log((Time.time - lastSpawnTime).ToString() + " is less than " + combineNoteThreshold.ToString() + "   Last spawn time: " + (lastSpawnTime - 0f).ToString());

                if ((Time.time - lastSpawnTime) < combineNoteThreshold) //notes are very fast. combine notes
                {
                    //Debug.Log("Add to old note: " + queue[0].note.ToString());
                    Text oldText = lastSpawnedObj.GetComponentInChildren<Text>();
                    oldText.text = oldText.text + queue[0].note;
                    queue.RemoveAt(0);
                }
                else //notes are slow and deserve their own hit marker
                {
                    InstantiateHitMarker();
                }
            }
        }
        
        
    }

    private void InstantiateHitMarker()
    {
        string note = queue[0].note;
        int instrument = queue[0].instrument;

        //Debug.Log("Queue size: " + queue.Count.ToString());
        //Debug.Log("Boolean: " + (instrumentsToSpawn.Contains(instrument)).ToString());
        if (instrumentsToSpawn.Contains(instrument))
        {
            spawn(note);
            queue.RemoveAt(0);
        }
    }



    public void addQueue(int note, int instrument)
    {
        //Debug.Log(instrument + " is being added: " + instrumentsToSpawn.Contains(instrument).ToString());
        //Debug.Log("Notes in list: " + instrumentsToSpawn[0].ToString());
        if (instrumentsToSpawn.Contains(instrument))
        {
            //Debug.Log("add to queue: " + note.ToString() + "," + instrument.ToString());
            SpawnEvent spawnEvent = new SpawnEvent(note, instrument);
            if (spawnEvent.note != "")
                queue.Add(spawnEvent);
            //Debug.Log("WORKING NOW");
        }
    }

    public void spawn(string note)
    {
        GameObject obj = (GameObject)Instantiate(hitMarkerPrefab, spawnPoints[activePoint].transform.position, Quaternion.identity);
        Text text = obj.GetComponentInChildren<Text>();
        text.text = note;
        obj.transform.SetParent(parent.transform);
        incrementSpawnPointIndex();

        lastSpawnTime = Time.time;
        //Store last spawned note
        lastSpawnedObj = obj;
        NotesHitEventSystem.hitMarkers.Add(obj);
        //Debug.Log((lastSpawnedObj == null).ToString());
    }

    private void incrementSpawnPointIndex()
    {
        activePoint++;
        if (activePoint > spawnPoints.Length - 1)
            activePoint = 0;
    }


}
