﻿using UnityEngine;
using System.Collections;

public class InstrumentSelector : MonoBehaviour
{
    public MidiController midiController;
    public UICheckbox[] instruments;
    int page = 1;
    public float offsetPage2 = -3.6f;

    void changeInstrument()
    {
        if (midiController != null)
        {
            int newInstrument = getNewInstrument();
            if (newInstrument >= 0)
            {
                midiController.midiInstrument = newInstrument;
            }
        }
    }

    public void showPage1()
    {
        Debug.Log("Page1");
        if (page != 1)
            transform.position = new Vector3(0f, 0f, 0f);
        page = 1;
    }



    public void showPage2()
    {
        Debug.Log("Page2");
        if (page != 2)
            transform.position = new Vector3(offsetPage2, 0f, 0f);
        page = 2;
    }

    int getNewInstrument()
    {
        for (int i = 0; i < instruments.Length; i++)
        {
            if ((instruments[i] != null) && (instruments[i].isChecked))
                return i;
        }

        return -1;
    }
}
