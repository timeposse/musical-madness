﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitMarker : MonoBehaviour {
    public string letters = "";
    public float time = 0f;

    public HitMarker(string l, float t)
    {
        letters = l;
        time = t;
    }
}
