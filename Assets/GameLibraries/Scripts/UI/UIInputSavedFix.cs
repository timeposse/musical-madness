﻿using UnityEngine;
using System.Collections;

public class UIInputSavedFix : MonoBehaviour {

	// Use this for initialization
	void Awake () {
        string label = GetComponent<UILabel>().text;
        GetComponent<UIInputSaved>().text = label;
	}
}
