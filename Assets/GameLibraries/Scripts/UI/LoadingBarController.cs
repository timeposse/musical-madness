﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingBarController : MonoBehaviour {
    public Image bar;
    public Text text;
    String loading = "Loading...";
    String done = "Done!";

    void Start()
    {
        if (text == null)
        {
            throw new Exception("Loading Bar requires a Text type");
        }
        SetText(loading);
    }

    public void SetText(string message)
    {
        text.text = message;
    }

    
	public void SetProgress(float percent)
    {
        bar.fillAmount = percent;
        if (percent == 1f)
        {
            SetText(done);
        }
        else
        {
            SetText(loading + " " + ((int)(percent * 100)).ToString() + "%");
        }
        
    }
}
