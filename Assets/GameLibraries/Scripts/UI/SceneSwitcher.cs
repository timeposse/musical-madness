﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour {
    public void GoToCrawler()
    {
        Debug.Log("Download");
        SceneManager.LoadScene("Download");
    }

    public void GoToPlaybackMidi()
    {
        Debug.Log("PlaybackMidi");
        SceneManager.LoadScene("PlaybackMidi");
    }

    public void GoToTitle()
    {
        Debug.Log("To Title");
        SceneManager.LoadSceneAsync("TitleScreen");
    }

    public void GoToPlayRoom()
    {
        SceneManager.LoadSceneAsync("PlayRoom");
    }
}
