﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanLeft : MonoBehaviour {
    public float speed = 0.1f;

    void Update()
    {
        Vector3 pos = transform.localPosition;
        pos.x -= speed * Time.deltaTime;
        transform.localPosition = pos;
    }
}
