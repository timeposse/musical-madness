﻿using UnityEngine;
using System.Collections;

public class DisplaySymphonyName : MonoBehaviour {
    public bool initFromGameState = true;

    void Start()
    {
        if (initFromGameState)
        {
            UILabel label = GetComponent<UILabel>();
            if (label != null)
            {
                GameObject gamestate = GameObject.Find("GameState");
                if (gamestate != null)
                {
                    label.text = SymphonyName.name;
                }
            }
            else
            {
                Debug.Log("UILabel is null");
            }
        }
    }
}
