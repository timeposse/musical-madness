﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Score : MonoBehaviour {
    public static int points = 0;
    Text score;

    void Start()
    {
        score = GetComponentInChildren<Text>();
        if (score == null)
        {
            throw new Exception("Score is a required component for Score object");
        }
    }

    public static void setPoints(int newPt)
    {
        points = newPt;
    }

    void Update()
    {
        score.text = points.ToString();
    }
}
