﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Highlight : MonoBehaviour {
    public GameObject highlight;

    void Update()
    {
        ConditionalDestroy conditions = GetComponent<ConditionalDestroy>();
        if (conditions.inZone && conditions.hittable)
        {
            highlight.active = true;
        }
        else
        {
            highlight.active = false;
        }
    }
}
