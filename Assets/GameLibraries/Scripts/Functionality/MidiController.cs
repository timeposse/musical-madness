﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CSharpSynth.Effects;
using CSharpSynth.Sequencer;
using CSharpSynth.Synthesis;
using CSharpSynth.Midi;
using System;


[RequireComponent(typeof(AudioSource))]
public class MidiController : MonoBehaviour
{
    //Temp Dev variables
    public NoteText noteText;
    public bool conversion = false;

    //Public
    //Try also: "FM Bank/fm" or "Analog Bank/analog" for some different sounds
    public string bankFilePath = "GM Bank/gm";
    public int bufferSize = 1024;
    public int midiNote = 60; //Ranges from 0-255
    public int midiNoteVolume = 100;
    public int midiInstrument = 1;
    public string filename = "Midis/hebe.mid";
    public int selectedInstrument = 0;
    public bool recordingRoom = false;

    //Private 
    float now = 0f;
    float startTime = 0f;
    private float[] sampleBuffer;
    private float gain = 1f;
    private StreamSynthesizer midiStreamSynthesizer;
    private MidiSequencer midiSequencer;
    
    private string keyboardLetters = "";
    private string lastKeyboardLetters = "";
    List<NoteInfo> notesOn = new List<NoteInfo>();
    List<NoteInfo> notesOff = new List<NoteInfo>();
    public bool playSelectedInstruments = false;
    public bool playNonSelectedInstruments = true;
    public bool isolateInstruments = true;
    public bool muteFlag = false;
    public bool initFromGameState = false;
    public bool spawnNotes = false;


    //Downstream dependencies
    public NoteSpawner noteSpawner;


    void Awake()
    {
        if (initFromGameState)
        {
            midiInstrument = InstrumentSelection.instrument;
            midiNoteVolume = InstrumentSelection.volume;
        }

        midiStreamSynthesizer = new StreamSynthesizer(44100, 2, bufferSize, 40);
        sampleBuffer = new float[midiStreamSynthesizer.BufferSize];
        midiStreamSynthesizer.LoadBank(bankFilePath);
        
        midiSequencer = new MidiSequencer(midiStreamSynthesizer);
        //midiSequencer.mute = true;
        //this.toggleMute();
        LoadMidiFile(filename);
        //midiSequencer.Play();
    }

    public void LoadMidiFile(string filename)
    {
        Debug.Log("Loading Midi: " + filename);

        midiSequencer.LoadMidi(filename, false);
    }

    void printInstrument()
    {
        Debug.Log(Enum.GetName(typeof(InstrumentEnum), selectedInstrument));
        
    }

    public void playNote(int note)
    {
        if (recordingRoom)
        {
            if (!muteFlag)
            {
                midiStreamSynthesizer.NoteOn(1, note, midiNoteVolume, midiInstrument);
            }
                
        }
        else
        {
            if ((noteSpawner != null) && spawnNotes)
            {
                //Debug.Log("Spawned");
                if (Recorder.record)
                {
                    Recorder.recordNote("temp", 1, note, midiNoteVolume, midiInstrument);
                }
                noteSpawner.spawnNote(1, note, midiNoteVolume, midiInstrument);
            }
            //Logger.logInput("Play Note: " + note);

            if (!muteFlag)
            {
                Logger.logStatus("Instrument:" + midiInstrument + ",Play Note: " + note + ",Volume: " + midiNoteVolume);
                midiStreamSynthesizer.NoteOn(1, note, midiNoteVolume, midiInstrument);
            }
                

            //midiSequencer.Play();
        }
    }

    public void playNote(int channel, int note, int midiNoteVolume, int midiInstrument)
    {
        if ((noteSpawner != null) && spawnNotes)
        {
            noteSpawner.spawnNote(1, note, midiNoteVolume, midiInstrument);
        }

        if(!muteFlag)
        {
            midiStreamSynthesizer.NoteOn(channel, note, midiNoteVolume, midiInstrument);
        }
            
        //Logger.logStatus("Instrument:" + midiInstrument + ",Play Note: " + note + ",Volume: " + midiNoteVolume);
        //midiSequencer.Play();
        
    }

    void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.Space))
        {
            playBackMidi();
        }*/
        now = Time.time;
    }

    public void playBackMidi()
    {
        midiSequencer.Play();
        startTime = now;
    }

    public void stopPlayBackMidi()
    {
        midiSequencer.Stop(true);
    }

    public void toggleMute()
    {
        muteFlag = !muteFlag;

    }

    public void stopNote(int note)
    {
        if (Recorder.record)
        {
            Recorder.recordNote("temp", 1, note);
        }
        midiStreamSynthesizer.NoteOff(1, note);
    }

    public void stopNote(int channel, int note)
    {
        midiStreamSynthesizer.NoteOff(channel, note);
    }

    private void OnAudioFilterRead(float[] data, int channels)
    {
        MidiSequencerEvent seqEvents = midiStreamSynthesizer.GetNext(sampleBuffer);
        
        //Logger.logStatus("Processing New Events");

        if (seqEvents != null)
        {
            ClearTempVars();
            ProcessNewMidiEvents(seqEvents);

            string letter;
            List<NoteInfo> newNotes = new List<NoteInfo>();
            for (int on = 0; on < notesOn.Count; on++)
            {                
                NoteInfo note = notesOn[on];
                Logger.logStatus("Instrument:" + note.getInstrument() + ",Play Note: " + note.getNote() + ",Volume: " + note.getVolume());
                if (conversion)
                {
                    string message = "On," + note.getChannel().ToString() + "," + note.getNote().ToString() + "," + note.getVolume().ToString() + "," + note.getInstrument().ToString() + "," + (now - startTime).ToString();
                    Logger.logFile("Conversions/" + note.getInstrument().ToString() + ".txt", message);

                }
                
                if (note.getInstrument() == selectedInstrument)
                {   
                    letter = KeyNoteMap.ConvertKeyIndexToLetter(note.getNote() - 37);
                    keyboardLetters += letter;
                    newNotes.Add(note);
                    if (playSelectedInstruments)
                    {
                        playNote(note.getChannel(), note.getNote(), note.getVolume(), note.getInstrument());
                        
                    }
                    
                }
                else if (playNonSelectedInstruments)
                {
                    playNote(note.getChannel(), note.getNote(), note.getVolume(), note.getInstrument());
                }
                    
            }

            if (notesOn.Count > 0)
            {
                if (noteSpawner != null)
                {
                    noteSpawner.spawnNote(newNotes);
                }   
                else
                    Debug.Log("noteSpawner is null");
            }

            /*if (keyboardLetters != "")
            {
                noteText.setText(keyboardLetters);
                Logger.logStatus("Keyboard Letters: " + keyboardLetters);
            }*/

            List<int> finalNotesOff = new List<int>();
            for (int off = 0; off < notesOff.Count; off++)
            {
                NoteInfo note = notesOff[off];
                if (conversion)
                {
                    string message = "Off," + note.getChannel().ToString() + "," + note.getNote().ToString() + "," + (now - startTime).ToString();
                    Logger.logFile("Conversions/Off.txt", message);
                }
                
                finalNotesOff.Add( note.getNote() );
                stopNote(note.getChannel(), note.getNote());
            }

            /*if (finalNotesOff.Count > 0)
            {
                if (noteSpawner != null)
                    noteSpawner.spawnNoteOff(finalNotesOff);
                else
                    Debug.Log("noteSpawner is null");
            }*/

        }

        for (int i = 0; i < data.Length; i++)
        {
            data[i] = sampleBuffer[i] * gain;
        }
    }

    private void ProcessNewMidiEvents(MidiSequencerEvent seqEvents)
    {
        for (int eventIndex = 0; eventIndex < seqEvents.Events.Count; eventIndex++)
        {
            ProcessMidiEvent(seqEvents.Events[eventIndex]);
        }
    }

    private void ClearTempVars()
    {
        lastKeyboardLetters = keyboardLetters;
        keyboardLetters = "";
        notesOn.Clear();
        notesOff.Clear();
    }

    private void ProcessMidiEvent(MidiEvent midiEvent)
    {
        if (midiEvent.midiChannelEvent != MidiHelper.MidiChannelEvent.None)
        {
            switch (midiEvent.midiChannelEvent)
            {
                case MidiHelper.MidiChannelEvent.Program_Change:
                    if (midiEvent.channel != 9)
                    {
                        //Logger.logStatus("Program change");
                        //if (midiEvent.parameter1 < synth.SoundBank.InstrumentCount)
                        //    currentPrograms[midiEvent.channel] = midiEvent.parameter1;
                    }
                    else //its the drum channel
                    {
                        //Logger.logStatus("Program change");
                        //if (midiEvent.parameter1 < synth.SoundBank.DrumCount)
                        //    currentPrograms[midiEvent.channel] = midiEvent.parameter1;
                    }
                    break;
                case MidiHelper.MidiChannelEvent.Note_On:
                    //Logger.logStatus("Note On");
                    //channel,note,volume,instrument
                    NoteInfo noteOn = new NoteInfo(midiEvent.channel, midiEvent.parameter1, midiEvent.parameter2, midiSequencer.currentPrograms[midiEvent.channel], "Note_On");
                    notesOn.Add(noteOn);
                    /*if (blockList.Contains(midiEvent.channel))
                        //return midiEvent;
                    if (this.NoteOnEvent != null)
                        this.NoteOnEvent(midiEvent.channel, midiEvent.parameter1, midiEvent.parameter2);
                    if (!mute)
                        synth.NoteOn(midiEvent.channel, midiEvent.parameter1, midiEvent.parameter2, currentPrograms[midiEvent.channel]);*/
                    break;
                case MidiHelper.MidiChannelEvent.Note_Off:
                    //Logger.logStatus("Note Off");
                    NoteInfo noteOff = new NoteInfo(midiEvent.channel, midiEvent.parameter1, 0, 0, "Note_Off");
                    notesOff.Add(noteOff);
                    /*if (this.NoteOffEvent != null)
                        this.NoteOffEvent(midiEvent.channel, midiEvent.parameter1);
                    synth.NoteOff(midiEvent.channel, midiEvent.parameter1);*/
                    break;
                case MidiHelper.MidiChannelEvent.Pitch_Bend:
                    //Logger.logStatus("Pitch Bend");
                    //Store PitchBend as the # of semitones higher or lower
                    //synth.TunePositions[midiEvent.channel] = (double)midiEvent.Parameters[1] * PitchWheelSemitoneRange;
                    break;
                case MidiHelper.MidiChannelEvent.Controller:
                    switch (midiEvent.GetControllerType())
                    {
                        case MidiHelper.ControllerType.AllNotesOff:
                            //Logger.logStatus("All Notes Off");
                            //synth.NoteOffAll(true);
                            break;
                        case MidiHelper.ControllerType.MainVolume:
                            //Logger.logStatus("Main Volume");
                            //synth.VolPositions[midiEvent.channel] = midiEvent.parameter2 / 127.0f;
                            break;
                        case MidiHelper.ControllerType.Pan:
                            //Logger.logStatus("Pan");
                            //synth.PanPositions[midiEvent.channel] = (midiEvent.parameter2 - 64) == 63 ? 1.00f : (midiEvent.parameter2 - 64) / 64.0f;
                            break;
                        case MidiHelper.ControllerType.ResetControllers:
                            //Logger.logStatus("Reset Controllers");
                            //ResetControllers();
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
        else
        {
            switch (midiEvent.midiMetaEvent)
            {
                case MidiHelper.MidiMetaEvent.Tempo:
                    //Logger.logStatus("Tempo Change");
                    //_MidiFile.BeatsPerMinute = MidiHelper.MicroSecondsPerMinute / System.Convert.ToUInt32(midiEvent.Parameters[0]);
                    break;
                default:
                    break;
            }
        }
        //return midiEvent;
    }

}
