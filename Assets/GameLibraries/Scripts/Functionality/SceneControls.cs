﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class SceneControls : MonoBehaviour {
    public void SwitchToPlayScene()
    {
        InstrumentSelection.instrument = GameObject.Find("MidiController").GetComponent<MidiController>().midiInstrument;
        InstrumentSelection.volume = GameObject.Find("MidiController").GetComponent<MidiController>().midiNoteVolume;
        Debug.Log("Switch to Play Scene");
        SceneManager.LoadScene("PlayRoom", LoadSceneMode.Single);
    }

    public void SwitchToInstrumentSelect()
    {
        Debug.Log("Switch to Instrument Select Scene");
        SceneManager.LoadScene("InstrumentSelect", LoadSceneMode.Single);
    }

    public void SwitchToListeningSongSelect()
    {
        Debug.Log("Switch to Instrument Select Scene");
        SceneManager.LoadScene("ListeningSongSelect", LoadSceneMode.Single);
    }

    public void GoToModeSelect()
    {
        Debug.Log("Switch to Mode Select Scene");
        SceneManager.LoadScene("ModeSelect", LoadSceneMode.Single);
    }

    public void SwitchToSymphonySelect()
    {
        Debug.Log("Switch to SymphonySongSelect Scene");
        SceneManager.LoadScene("SymphonySongSelect", LoadSceneMode.Single);
    }

    public void SwitchToSymphonyPlay()
    {
        try
        {
            InstrumentSelection.instrument = GameObject.Find("MidiController").GetComponent<MidiController>().midiInstrument;
            InstrumentSelection.volume = GameObject.Find("MidiController").GetComponent<MidiController>().midiNoteVolume;
        }
        catch (NullReferenceException e)
        {
            InstrumentSelection.instrument = 0;
            Debug.Log("Bypassing instrument loading");
        }
        Debug.Log("Switch to SymphonyPlayRoom Scene");
        SceneManager.LoadScene("SymphonyPlayRoom", LoadSceneMode.Single);
    }

    public void SwitchToConversion()
    {
        Debug.Log("Switch to Conversion Room");
        SceneManager.LoadScene("MidiConversionRoom", LoadSceneMode.Single); 
    }

    public void GoToSymphonyInstrumentSelect()
    {
        Debug.Log("Switch to Symphony Instrument Select Scene");
        SceneManager.LoadScene("SymphonyInstrumentSelect", LoadSceneMode.Single);    
    }

    public void GoToPracticeRoom()
    {
        Debug.Log("Switch to PracticeRoom Scene");
        SceneManager.LoadScene("PracticeRoom", LoadSceneMode.Single);
    }

    public static string getCurrentScene()
    {
        return SceneManager.GetActiveScene().name;
    }
}
