﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotesHitEventSystem : MonoBehaviour {
    string keysDown = "";


    public bool debugKeyInput = false;
    public bool debugHitMarkers = false;

    public static List<GameObject> hitMarkers = new List<GameObject>();
    public List<GameObject> markers = new List<GameObject>();

    public void register(string noteHit)
    {
        Debug.Log(noteHit + " check if contain in [" + keysDown + "]");
        
        if (!keysDown.Contains(noteHit))
        {
            keysDown += noteHit;
            if (debugKeyInput)
                Debug.Log("Notes Hit: " + keysDown);
        }
        
        
    }

    public void unregister(string noteHit)
    {
        keysDown = keysDown.Replace(noteHit, string.Empty);

        if (debugKeyInput)
            Debug.Log("Notes Hit: " + keysDown);


    }

    static bool clearKeysDown = false;
    public static void unregisterAllKeys()
    {
        clearKeysDown = true;
    }


    void Update()
    {
        if (clearKeysDown)
        {
            keysDown = "";
            clearKeysDown = false;
        }
            

        if (debugHitMarkers)
            markers = hitMarkers;
        
        for (int i = 0; i < hitMarkers.Count; i++)
        {
            //Debug.Log("Keys down contain [" + keysDown + "]");
            ConditionalDestroy conditions = hitMarkers[0].GetComponent<ConditionalDestroy>();
            if (conditions.inZone && conditions.hittable)
            {
                //Debug.Log("Checking: " + conditions.inZone.ToString() + "," + conditions.hittable.ToString());
                conditions.AttemptDestroy(keysDown);
            }
        }
    }
}
