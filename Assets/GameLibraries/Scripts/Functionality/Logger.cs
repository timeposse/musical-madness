﻿using UnityEngine;
using System.Collections;
using System.IO;

public class Logger {
    public static int debugLevel = 4;

    public static void logMessage(string message)
    {
        if (debugLevel == 0)
            Debug.Log(message);
    }

    public static void logStatus(string message)
    {
        if (debugLevel == 1)
            Debug.Log(message);
    }

    public static void logInput(string message)
    {
        if (debugLevel == 9)
            Debug.Log(message);
    }

    public static void logFile(string file, string message)
    {
        Debug.Log(message);
        using (StreamWriter w = File.AppendText(file))
        {
            w.WriteLine(message + "\n");
        }
    }
}
