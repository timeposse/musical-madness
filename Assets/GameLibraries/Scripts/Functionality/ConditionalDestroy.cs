﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConditionalDestroy : MonoBehaviour {
    public bool inZone = false;
    public bool notesHit = false;
    public bool hittable = false;

    public Text text;

    void Start()
    {
        getText();
    }

    void getText()
    {
        text = GetComponentInChildren<Text>();
        //Debug.Log("Text found " + text.text);
        
    }

    public void AttemptDestroy(string playedLetters)
    {
        //Debug.Log("Played Letters: " + playedLetters);
        if (hittable)
        {
            //Debug.Log("Hittable");
            for (int i = 0; i < playedLetters.Length; i++)
            {
                //Debug.Log("Testing: " + playedLetters[i]);
                //Debug.Log("Text before: " + text.text);
                text.text = text.text.Replace(playedLetters[i].ToString(), string.Empty);
                //Debug.Log("Text after: " + text.text);    
            }
        }
        
    }

    
    void Update()
    {
        if (text == null)
        {
            getText();
        }

        if (!hittable)
        {
            //Debug.Log(this.gameObject.GetInstanceID().ToString() + " == " + NotesHitEventSystem.hitMarkers[0].GetInstanceID().ToString());
            if (this.gameObject.GetInstanceID() == NotesHitEventSystem.hitMarkers[0].GetInstanceID())
            {
                hittable = true;   
            }
        }

        if (hittable && text.text.Replace(" ", string.Empty).Length == 0)
        {
            notesHit = true;
            Score.setPoints(Score.points + 100);
        }

        if (inZone && notesHit)
        {
            GameObject.Destroy(this.gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        //Debug.Log("Collision Enter");
        if (collider.gameObject.tag == "HitZone") 
            inZone = true;

        if (collider.gameObject.tag == "Destroy")
        {
            Score.setPoints(Score.points - 500);
            GameObject.Destroy(this.gameObject);
        }
            
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "HitZone") 
            inZone = false;
    }

}
