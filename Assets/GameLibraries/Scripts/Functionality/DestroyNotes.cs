﻿using UnityEngine;
using System.Collections;

public class DestroyNotes : MonoBehaviour {
    string chord = "";

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "NoteOn")
        {
            NoteText notetext = other.gameObject.GetComponentInChildren<NoteText>();
            if (notetext != null)
            {
                chord = notetext.getText();
                //Debug.Log("Note Destroyed: " + chord);

                Destroy(other.gameObject);
            }
        }
    }
}
