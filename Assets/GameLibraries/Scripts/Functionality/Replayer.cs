﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class Replayer : MonoBehaviour {
    public static string songLocation = "";
    bool songLoaded = false;
    bool songFinished = false;
    StreamReader reader;
    List<string> songNotes = new List<string>();
    float startTime;
    int playIndex = 0;
    MidiController midiController;
    bool textFileLoaded = false;
    bool midiFileLoaded = false;

    void Start()
    {
        init();
    }

    void init()
    {
        songLoaded = false;
        songFinished = false;
        songNotes = new List<string>();
        songLocation = "";
        playIndex = 0;

        midiController = GameObject.Find("MidiController").GetComponent<MidiController>();
    }

    void Update()
    {
        if (!songLoaded && (songLocation != ""))
        {
            songNotes = LoadSong(songLocation);
            songLoaded = true;
            startTime = Time.time;
        }
        else if (songLoaded && textFileLoaded)
        {
            PlayTextFile();
        }
        else if (songLoaded && midiFileLoaded)
        {
            PlayMidiFile();
        }
    }

    private void PlayTextFile()
    {
        if (!songFinished)
        {
            for (int i = playIndex; i < songNotes.Count; i++)
            {
                //Debug.Log("Play Index: " + playIndex.ToString());
                if (songNotes[i] != "")
                {
                    bool breakLoop = ProcessNote(songNotes[i]);
                    if (breakLoop)
                    {
                        break;
                    }
                }
                    
            }
            if (playIndex >= songNotes.Count - 1)
                songFinished = true;
        }
    }

    private void PlayMidiFile()
    {
        if(!songFinished)
        {
            midiController.playBackMidi();
            songFinished = true;
        }
    }

    bool isTxt(string filename)
    {
        if (filename.Contains(".txt"))
        {
            return true;
        }
        return false;
    }

    bool isMidi(string filename)
    {
        if (filename.Contains(".mid"))
        {
            return true;
        }
        return false;
    }

    bool ProcessNote(string noteString)
    {
        //Debug.Log("Processing Note: " + noteString);
        string[] data = noteString.Split(',');
        if (data[0] == "On")
        {
            float noteTime = float.Parse(data[5]);
            if (noteTime > (Time.time - startTime))
            {
                //Debug.Log("Not ready yet");
                return true;
            }
            else
            {
                //Debug.Log("Play");
                int channel = int.Parse(data[1]);
                int note = int.Parse(data[2]);
                int volume = int.Parse(data[3]);
                int instrument = int.Parse(data[4]);
                if (midiController != null)
                {
                    midiController.playNote(channel, note, volume, instrument);
                    playIndex += 1;
                }
                else
                {
                    Debug.Log("Error: midiController is null");
                }
                return false;
            }
        }
        else// if (data[0] == "Off")
        {
            float noteTime = float.Parse(data[3]);
            if (noteTime > (Time.time - startTime))
            {
                return true;
            }
            else
            {
                int channel = int.Parse(data[1]);
                int note = int.Parse(data[2]);
                if (midiController != null)
                {
                    midiController.stopNote(channel, note);
                    playIndex += 1;
                }
                else
                {
                    Debug.Log("Error: midiController is null");
                }
                return false;
            }
        }
    }

    public List<string> LoadSong(string path)
    {
        Debug.Log("Path: " + path);
        List<string> output = new List<string>();

        if (path == null)
            return output;

        if (isTxt(path) && !isMidi(path))
        {
            Debug.Log("Loading text file");
            textFileLoaded = true;
            reader = new StreamReader(path);
            using (reader)
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    output.Add(line);
                }
            }
        }
        else
        {
            midiFileLoaded = true;
            string midiSongLocation = songLocation.Remove(songLocation.Length - 4);
            Debug.Log("Loading midi file: " + midiSongLocation);
            midiController.LoadMidiFile(midiSongLocation);
        }

        songNotes = output;
        return output;
    }

}
