﻿using UnityEngine;
using System.IO;

public class CreateFolder : MonoBehaviour
{
    string tmpdir = "Symphonies/tmp";

    void Awake()
    {
        tmpdir = "Symphonies/tmp";
    }

	void CreateNewSymphony()
    {        
        if (SongTitle.title != "")
        {
            tmpdir = "Symphonies/" + SongTitle.title;
            var folder = Directory.CreateDirectory(tmpdir);
            SymphonyName.name = SongTitle.title;
        }
        else
        {
            System.IO.Directory.Delete(tmpdir, true);
            var folder = Directory.CreateDirectory(tmpdir);

        }

        CreateMasterFile();

        SceneControls control = GetComponent<SceneControls>();
        if (control != null)
        {
            control.SwitchToSymphonyPlay();
        }
    }

    void RenameSymphony()
    {

    }

    void CreateMasterFile()
    {
        Debug.Log("Create Master File");
        using (StreamWriter writer = new StreamWriter(tmpdir + "/master_symphony.txt"))
        {
            string empty = "";
            writer.Write(empty);
            writer.Close();
        }
    }

}
