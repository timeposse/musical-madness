﻿using UnityEngine;
using System.Collections;
using System.IO;

public class Recorder : MonoBehaviour {
    public static bool record = true;
    string lastFileName = "temp";
    static string songDir = "Songs/";
    static float startTime;
    static bool playedFirstNote = false;
    public bool symphonyRecorder = false;

    void Awake()
    {
        Debug.Log("Init Recorder");

        setSongDirectory();
        init();
    }

    private static void init()
    {
        playedFirstNote = false;
        ClearTempFile();
    }

    private void setSongDirectory()
    {
        if (symphonyRecorder)
        {
            songDir = "Symphonies/" + SymphonyName.name + "/";
        }
        else
        {
            songDir = "Songs/";
        }
    }

    private static void ClearTempFile()
    {
        using (StreamWriter writer = new StreamWriter(songDir + "temp.txt"))
        {
            string noteOnMessage = "";
            writer.Write(noteOnMessage);
            writer.Close();
        }
    }

    public static void recordNote(string filename, int channel, int note, int midiNoteVolume, int midiInstrument)
    {
        if (playedFirstNote == false)
        {
            startTime = Time.time;
            playedFirstNote = true;
            Debug.Log("First Note Played");
        }
        using (StreamWriter writer = new StreamWriter(songDir + filename + ".txt", true))
        {
            string noteOnMessage = "On";
            float songTime = Time.time - startTime;
            noteOnMessage += "," + channel.ToString() + "," + note.ToString() + "," + midiNoteVolume.ToString() + "," + midiInstrument.ToString() + "," + songTime.ToString();
            writer.WriteLine(noteOnMessage);
            writer.Close();
        }
    }

    public static void recordNote(string filename, int channel, int note)
    {
        using (StreamWriter writer = new StreamWriter(songDir + filename + ".txt", true))
        {
            string noteOffMessage = "Off";
            float songTime = Time.time - startTime;
            noteOffMessage += "," + channel.ToString() + "," + note.ToString() + "," + songTime.ToString();
            writer.WriteLine(noteOffMessage);
            writer.Close();
        }
    }

    public void saveNewSong()
    {
        string origfilename = lastFileName;
        string filename = SongTitle.title;
        Debug.Log("Saved Song " + filename);
        try
        {
            System.IO.File.Move(songDir + origfilename + ".txt", songDir + filename + ".txt");
            lastFileName = filename;
        }
        catch (IOException e)
        {
            Debug.Log(e);
        }
    }

    public void saveNewSongInSymphony()
    {
        string symphdir = "Symphonies/" + SymphonyName.name + "/";
        string origfilename = lastFileName;
        string filename = SongTitle.title;
        Debug.Log("Saved Song " + filename);
        try
        {
            System.IO.File.Move(songDir + origfilename + ".txt", symphdir + filename + ".txt");
            lastFileName = filename;
        }
        catch (IOException e)
        {
            Debug.Log(e);
        }
    }
}
