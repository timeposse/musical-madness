﻿public class NoteEvent
{
    public enum eventType { NoteOff, NoteOn, Program_Change, Pitch_Bend, Controller};//, Sequence_Or_Track_Name, Sequencer_Specific_Event };  

    public int type;
    public int channel;
    public int parameter1;
    public int parameter2;
    public int program;
    public float time;

    public NoteEvent(int type, int channel, int p1, int p2, int prog, float t)
    {
        this.type = type;
        this.parameter1 = p1;
        this.parameter2 = p2;
        this.program = prog;
        this.time = t;
        this.channel = channel;
    }
}