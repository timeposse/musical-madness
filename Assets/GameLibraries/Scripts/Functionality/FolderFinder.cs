﻿using UnityEngine;

public class FolderFinder : MonoBehaviour
{
    public GUISkin newskin;
    protected string m_textPath;

    protected FileBrowser m_fileBrowser;

    [SerializeField]
    protected Texture2D m_directoryImage,
                        m_fileImage;

    protected void OnGUI()
    {
        GUI.skin = newskin;
        if (m_fileBrowser != null)
        {
            m_fileBrowser.OnGUI();
        }
        else
        {
            OnGUIMain();
        }
    }

    public float x;
    public float y;
    public float width;
    public float height;

    void Start()
    {
        SpawnMenu();
    }

    protected void OnGUIMain()
    {

        GUILayout.BeginHorizontal();
        GUILayout.Label("Text File", GUILayout.Width(100));
        GUILayout.FlexibleSpace();
        GUILayout.Label(m_textPath ?? "none selected");
        if (GUILayout.Button("...", GUILayout.ExpandWidth(false)))
        {
            SpawnMenu();
        }
        GUILayout.EndHorizontal();
    }

    private void SpawnMenu()
    {
        m_fileBrowser = new FileBrowser(
            new Rect(x, y, width, height),
            "Choose Text File",
            FileSelectedCallback
        );
        m_fileBrowser.SelectionPattern = "*.txt";
        m_fileBrowser.CurrentDirectory += "\\Songs";
        m_fileBrowser.DirectoryImage = m_directoryImage;
        m_fileBrowser.FileImage = m_fileImage;
    }



    protected void FileSelectedCallback(string path)
    {
        m_fileBrowser = null;
        m_textPath = path;
        Replayer.songLocation = m_textPath;
    }
}