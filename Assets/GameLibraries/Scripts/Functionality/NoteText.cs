﻿using UnityEngine;
using System.Collections;

public class NoteText : MonoBehaviour {
    string letters = "Brandon Ling 2016-11-25";
    string special = "!@#$%^&*()";
    string numbers = "0123456789";
    TextMesh textMesh;


    void Start()
    {
        textMesh = GetComponent<TextMesh>();
    }

	void Update() 
    {
        if (letters != textMesh.text)
            textMesh.text = letters;
    }
	
    public void setText(string newtext)
    {
        newtext = sortTextLowerCaseFirst(newtext);
        letters = newtext;
    }

    public string getText()
    {
        return letters;
    }

    private string sortTextLowerCaseFirst(string text)
    {
        string lower = text.ToLower();
        string upper = text.ToUpper();
        string head = "";
        string tail = "";

        for (int i = 0; i < text.Length; i++)
        {
            string letter = "" + text[i];
            if ((text[i] == upper[i]) || (special.Contains(letter)))
            {
                head += text[i];
            }
            else if ((text[i] == lower[i]) || (numbers.Contains(letter)))
            {
                tail += text[i];
            }   
        }

        return head + tail;
    }
}
