﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class SymphonyPlayback : MonoBehaviour 
{
    string symphonyName;
    public MidiController controller;
    public NoteSpawner noteSpawner;

    void Start()
    {
        symphonyName = SymphonyName.name; 
        string directory = "C:\\Users\\Meta\\Documents\\Musical Madness\\Symphonies\\" + symphonyName + "\\";
        string[] filePaths = Directory.GetFiles(@directory, "*.txt", SearchOption.TopDirectoryOnly);

        foreach (var item in filePaths)
        {
            string filename = item.ToString();
            if ((filename != directory + "temp.txt") && (filename != directory + "master_symphony.txt"))
            {
                SymphonyReplayer replayer = initReplayer(filename);
            }
            
        }
    }

    void stopAllReplayers()
    {
        GameObject[] notes = GameObject.FindGameObjectsWithTag("NoteOn");
        for (int i = 0; i < notes.Length; i++)
        {
            GameObject.Destroy(notes[i]);
        }

        SymphonyReplayer[] replayers = GetComponents<SymphonyReplayer>();
        for (int i = 0; i < replayers.Length; i++)
        {
            replayers[i].init();
        }
        noteSpawner.resetSpawnIndex();
        controller.stopPlayBackMidi();
    }

    void startAllReplayers()
    {
        SymphonyReplayer[] replayers = GetComponents<SymphonyReplayer>();
        for (int i = 0; i < replayers.Length; i++)
        {
            replayers[i].play = true;
        }

    }

    SymphonyReplayer initReplayer(string filename)
    {
        //Debug.Log(filename.ToString());
        SymphonyReplayer replayer = gameObject.AddComponent<SymphonyReplayer>();
        replayer.init();
        replayer.setSongName(filename, symphonyName);
        replayer.play = true;
        return replayer;
    }

    
}
