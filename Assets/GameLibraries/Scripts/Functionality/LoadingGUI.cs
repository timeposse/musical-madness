﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(SceneSwitcher))]
public class LoadingGUI : MonoBehaviour {
    public MidiDownloader downloader;
    public WebCrawler crawler;
    public SceneSwitcher scene;

    string hostname;
    string filename;

    public string url = "http://www.vgmusic.com/";

    void Start()
    {
        scene = GetComponent<SceneSwitcher>();

        if (!FoxHelper.debugMode)
            crawler.CrawlInBackground(url);
    }

    void Update()
    {
        if (downloader.progress == 1f)
        {
            scene.GoToPlayRoom();
        }
    }

    /*void OnGUI()
    {
        // Make a background box
        GUILayout.BeginArea(new Rect(Screen.width / 2 - 75, Screen.height / 2 - 50, 150, 300));


        if (GUILayout.Button("Crawl"))
        {

            try
            {
                crawler.CrawlInBackground(url);
                
                
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }

            //List<string> urls = crawler.CrawlRandom(url);
            
            
        }
        if (GUILayout.Button("Next"))
        {
            GetComponent<SceneSwitcher>().GoToPlaybackMidi();
        }

        GUILayout.EndArea();

    }*/
}
