﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour 
{
    public static bool disabled = false;
    MidiPlayer midiController;

    void Start()
    {
        midiController = GetComponentInChildren<MidiPlayer>();
    }

    void Update()
    {
        if (!disabled)
        {
            HandleDownInputs();
            HandleUpInputs();
        }
    }

    void SendDownNoteToMidiController(int keyIndex)
    {
        keyIndex += 37; //Index offset for midi library
        if (midiController != null)
        {
            midiController.playNote(keyIndex);
        }
    }

    void SendUpNoteToMidiController(int keyIndex)
    {
        keyIndex += 37; //Index offset for midi library
        if (midiController != null)
        {
            midiController.stopNote(keyIndex);
        }
    }

    void HandleUpInputs()
    {
        //First row of keyboard
        if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            SendUpNoteToMidiController(0);
            SendUpNoteToMidiController(1);
        }
        if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            SendUpNoteToMidiController(2);
            SendUpNoteToMidiController(3);
        }
        if (Input.GetKeyUp(KeyCode.Alpha3))
        {
            SendUpNoteToMidiController(4);
        }
        if (Input.GetKeyUp(KeyCode.Alpha4))
        {
            SendUpNoteToMidiController(5);
            SendUpNoteToMidiController(6);
        }
        if (Input.GetKeyUp(KeyCode.Alpha5))
        {
            SendUpNoteToMidiController(7);
            SendUpNoteToMidiController(8);
        }
        if (Input.GetKeyUp(KeyCode.Alpha6))
        {
            SendUpNoteToMidiController(9);
            SendUpNoteToMidiController(10);
        }
        if (Input.GetKeyUp(KeyCode.Alpha7))
        {
            SendUpNoteToMidiController(11);
        }
        if (Input.GetKeyUp(KeyCode.Alpha8))
        {
            SendUpNoteToMidiController(12);
            SendUpNoteToMidiController(13);
        }
        if (Input.GetKeyUp(KeyCode.Alpha9))
        {
            SendUpNoteToMidiController(14);
            SendUpNoteToMidiController(15);
        }
        if (Input.GetKeyUp(KeyCode.Alpha0))
        {
            SendUpNoteToMidiController(16);
        }

        //Second row of keyboard
        if (Input.GetKeyUp(KeyCode.Q))
        {
            SendUpNoteToMidiController(17);
            SendUpNoteToMidiController(18);
        }
        if (Input.GetKeyUp(KeyCode.W))
        {
            SendUpNoteToMidiController(19);
            SendUpNoteToMidiController(20);
        }
        if (Input.GetKeyUp(KeyCode.E))
        {
            SendUpNoteToMidiController(21);
            SendUpNoteToMidiController(22);
        }
        if (Input.GetKeyUp(KeyCode.R))
        {
            SendUpNoteToMidiController(23);
        }
        if (Input.GetKeyUp(KeyCode.T))
        {
            SendUpNoteToMidiController(24);
            SendUpNoteToMidiController(25);
        }
        if (Input.GetKeyUp(KeyCode.Y))
        {
            SendUpNoteToMidiController(26);
            SendUpNoteToMidiController(27);
        }
        if (Input.GetKeyUp(KeyCode.U))
        {
            SendUpNoteToMidiController(28);
        }
        if (Input.GetKeyUp(KeyCode.I))
        {
            SendUpNoteToMidiController(29);
            SendUpNoteToMidiController(30);
        }
        if (Input.GetKeyUp(KeyCode.O))
        {
            SendUpNoteToMidiController(31);
            SendUpNoteToMidiController(32);
        }
        if (Input.GetKeyUp(KeyCode.P))
        {
            SendUpNoteToMidiController(33);
            SendUpNoteToMidiController(34);
        }

        //Third row of keyboard
        if (Input.GetKeyUp(KeyCode.A))
        {
            SendUpNoteToMidiController(35);
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            SendUpNoteToMidiController(36);
            SendUpNoteToMidiController(37);
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            SendUpNoteToMidiController(38);
            SendUpNoteToMidiController(39);
        }
        if (Input.GetKeyUp(KeyCode.F))
        {
            SendUpNoteToMidiController(40);
        }
        if (Input.GetKeyUp(KeyCode.G))
        {
            SendUpNoteToMidiController(41);
            SendUpNoteToMidiController(42);
        }
        if (Input.GetKeyUp(KeyCode.H))
        {
            SendUpNoteToMidiController(43);
            SendUpNoteToMidiController(44);
        }
        if (Input.GetKeyUp(KeyCode.J))
        {
            SendUpNoteToMidiController(45);
            SendUpNoteToMidiController(46);
        }
        if (Input.GetKeyUp(KeyCode.K))
        {
            SendUpNoteToMidiController(47);
        }
        if (Input.GetKeyUp(KeyCode.L))
        {
            SendUpNoteToMidiController(48);
            SendUpNoteToMidiController(49);
        }

        //Fourth row of keyboard
        if (Input.GetKeyUp(KeyCode.Z))
        {
            SendUpNoteToMidiController(50);
            SendUpNoteToMidiController(51);
        }
        if (Input.GetKeyUp(KeyCode.X))
        {
            SendUpNoteToMidiController(52);
        }
        if (Input.GetKeyUp(KeyCode.C))
        {
            SendUpNoteToMidiController(53);
            SendUpNoteToMidiController(54);
        }
        if (Input.GetKeyUp(KeyCode.V))
        {
            SendUpNoteToMidiController(55);
            SendUpNoteToMidiController(56);
        }
        if (Input.GetKeyUp(KeyCode.B))
        {
            SendUpNoteToMidiController(57);
            SendUpNoteToMidiController(58);
        }
        if (Input.GetKeyUp(KeyCode.N))
        {
            SendUpNoteToMidiController(59);
        }
        if (Input.GetKeyUp(KeyCode.M))
        {
            SendUpNoteToMidiController(60);
        }
    }

    void HandleDownInputs()
    {
        //First row of keyboard
        if (Input.GetKeyDown(KeyCode.Alpha1) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha1) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(2);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(3);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(4);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(5);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(6);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(7);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(8);
        }
        if (Input.GetKeyDown(KeyCode.Alpha6) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(9);
        }
        if (Input.GetKeyDown(KeyCode.Alpha6) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(10);
        }
        if (Input.GetKeyDown(KeyCode.Alpha7) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(11);
        }
        if (Input.GetKeyDown(KeyCode.Alpha8) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(12);
        }
        if (Input.GetKeyDown(KeyCode.Alpha8) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(13);
        }
        if (Input.GetKeyDown(KeyCode.Alpha9) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(14);
        }
        if (Input.GetKeyDown(KeyCode.Alpha9) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(15);
        }
        if (Input.GetKeyDown(KeyCode.Alpha0) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(16);
        }

        //Second row of keyboard
        if (Input.GetKeyDown(KeyCode.Q) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(17);
        }
        if (Input.GetKeyDown(KeyCode.Q) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(18);
        }
        if (Input.GetKeyDown(KeyCode.W) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(19);
        }
        if (Input.GetKeyDown(KeyCode.W) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(20);
        }
        if (Input.GetKeyDown(KeyCode.E) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(21);
        }
        if (Input.GetKeyDown(KeyCode.E) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(22);
        }
        if (Input.GetKeyDown(KeyCode.R) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(23);
        }
        if (Input.GetKeyDown(KeyCode.T) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(24);
        }
        if (Input.GetKeyDown(KeyCode.T) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(25);
        }
        if (Input.GetKeyDown(KeyCode.Y) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(26);
        }
        if (Input.GetKeyDown(KeyCode.Y) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(27);
        }
        if (Input.GetKeyDown(KeyCode.U) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(28);
        }
        if (Input.GetKeyDown(KeyCode.I) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(29);
        }
        if (Input.GetKeyDown(KeyCode.I) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(30);
        }
        if (Input.GetKeyDown(KeyCode.O) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(31);
        }
        if (Input.GetKeyDown(KeyCode.O) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(32);
        }
        if (Input.GetKeyDown(KeyCode.P) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(33);
        }
        if (Input.GetKeyDown(KeyCode.P) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(34);
        }

        //Third row of keyboard
        if (Input.GetKeyDown(KeyCode.A) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(35);
        }
        if (Input.GetKeyDown(KeyCode.S) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(36);
        }
        if (Input.GetKeyDown(KeyCode.S) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(37);
        }
        if (Input.GetKeyDown(KeyCode.D) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(38);
        }
        if (Input.GetKeyDown(KeyCode.D) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(39);
        }
        if (Input.GetKeyDown(KeyCode.F) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(40);
        }
        if (Input.GetKeyDown(KeyCode.G) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(41);
        }
        if (Input.GetKeyDown(KeyCode.G) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(42);
        }
        if (Input.GetKeyDown(KeyCode.H) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(43);
        }
        if (Input.GetKeyDown(KeyCode.H) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(44);
        }
        if (Input.GetKeyDown(KeyCode.J) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(45);
        }
        if (Input.GetKeyDown(KeyCode.J) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(46);
        }
        if (Input.GetKeyDown(KeyCode.K) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(47);
        }
        if (Input.GetKeyDown(KeyCode.L) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(48);
        }
        if (Input.GetKeyDown(KeyCode.L) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(49);
        }

        //Fourth row of keyboard
        if (Input.GetKeyDown(KeyCode.Z) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(50);
        }
        if (Input.GetKeyDown(KeyCode.Z) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(51);
        }
        if (Input.GetKeyDown(KeyCode.X) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(52);
        }
        if (Input.GetKeyDown(KeyCode.C) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(53);
        }
        if (Input.GetKeyDown(KeyCode.C) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(54);
        }
        if (Input.GetKeyDown(KeyCode.V) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(55);
        }
        if (Input.GetKeyDown(KeyCode.V) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(56);
        }
        if (Input.GetKeyDown(KeyCode.B) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(57);
        }
        if (Input.GetKeyDown(KeyCode.B) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(58);
        }
        if (Input.GetKeyDown(KeyCode.N) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(59);
        }
        if (Input.GetKeyDown(KeyCode.M) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
        {
            SendDownNoteToMidiController(60);
        }
    }

}
