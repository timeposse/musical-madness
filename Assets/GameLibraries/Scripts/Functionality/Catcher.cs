﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Catcher : MonoBehaviour {
    public MidiController midiController;
    string chord = "";
    //string notesToStop = "";
    public List<int> notesToStop = new List<int>();

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "NoteOn")
        {
            NoteText notetext = other.gameObject.GetComponentInChildren<NoteText>();
            if (notetext != null)
            {
                chord = notetext.getText();
                if (midiController != null)
                {
                    Debug.Log("Chord played: " + chord);
                   
                    for (int i = 0; i < chord.Length; i++)
                    {
                        int index = KeyNoteMap.ConvertLetterToKeyIndex(chord[i]) + 37;
                        midiController.playNote(index);
                    }
                }
            }
        }
        else if (other.tag == "NoteOff")
        {
            NoteText notetext = other.gameObject.GetComponentInChildren<NoteText>();
            if (notetext != null)
            {
                int killNote = Int32.Parse(notetext.getText());

                Debug.Log("Stop Note: " + killNote);
                midiController.stopNote(killNote);
            }
        }
    }

    void Update()
    {
        if (notesToStop.Count > 0)
        {
            for (int i = 0; i < notesToStop.Count; i++)
            {
                midiController.stopNote(notesToStop[i]);
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        /*for (int i = 0; i < notesToStop.Length; i++)
        {
            //Debug.Log("Chord Stop: " + notesToStop[i].ToString());
            int index = KeyNoteMap.ConvertLetterToKeyIndex(notesToStop[i]) + 37;
            midiController.stopNote(1, index);
        }
        notesToStop = "";*/
    }
}
