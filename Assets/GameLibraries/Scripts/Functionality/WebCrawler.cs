﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Text.RegularExpressions;
using System.IO;
using System;
using System.Threading; 

[RequireComponent(typeof(MidiDownloader))]
public class WebCrawler : MonoBehaviour {
    MidiDownloader downloader;
    public List<string> urls = new List<string>();
    int filesFound = 0;
    string origHostName = "";

    bool isCrawling = false;
    bool _threadRunning;
    Thread _thread;
    bool workDone;
    string threadStartHostName = "";
    public string finalCrawledURL = "";
    public string lastCrawledMidi = "";

    public void CrawlInBackground(string hostname)
    {
        finalCrawledURL = "";

        threadStartHostName = hostname;
        if (!isCrawling)
        {
            isCrawling = true;

            // Begin our heavy work on a new thread.
            _thread = new Thread(ThreadedWork);
            _thread.Start();
        }
        else
        {
            Debug.Log("Crawling already in progress");
        }
    }

    private void killThread()
    {
        Debug.Log("Killing background crawl thread");
        workDone = true; //kill background thread on editor stop
        _threadRunning = false;
        isCrawling = false;
    }

    void ThreadedWork()
    {
        _threadRunning = true;
        workDone = false;

        // This pattern lets us interrupt the work at a safe point if neeeded.
        while (_threadRunning && !workDone)
        {
            // Do Work...
            FindRandomMidiFile(threadStartHostName);
            killThread();
        }
        _threadRunning = false;
    }




    int recursionLimit = 500;
    public string FindRandomMidiFile(string hostname)
    {
        Debug.Log("Start Crawl");
        string url = hostname;
        origHostName = hostname; //Backup hostname for crawler

        Debug.Log("Host set to " + url);

        int crawlIter = 0;
        while (!url.EndsWith(".mid"))
        {
            if (crawlIter < recursionLimit)
            {
                List<string> crawledLinks = Crawl(url);
                if (crawledLinks.Count == 1)
                {
                    url = hostname;
                    Debug.Log("Host reset to " + url);
                }
                else if (crawledLinks.Count > 0)
                {
                    System.Random rnd = new System.Random();
                    int index = rnd.Next(0, crawledLinks.Count);
                    //int index = UnityEngine.Random.Range(0, crawledLinks.Count);

                    url = crawledLinks[index];

                    Debug.Log("Picking random URL from " + crawledLinks.Count);
                    Debug.Log("Random index: " + index);
                    Debug.Log(url);

                }
                else if (url == hostname)
                {
                    Debug.Log("No links found on homepage");
                    throw new Exception("No links found on homepage");
                }
                else
                {
                    url = hostname;
                    Debug.Log("Host reset to " + url);
                }
            }
            else
            {
                Debug.Log("Crawler exited due to recursion limit");
                break;
            }

            crawlIter++;
        }
        finalCrawledURL = url;
        lastCrawledMidi = url;
        return url;
    }

    public List<string> Crawl(string hostname)
    {
        string expr = "href\\s*=\\s*(?:\"(?<1>[^\"]*)\"|(?<1>\\S+))";

        filesFound = 0;
        urls.Clear();

        urls.Add(hostname);
        using (WebClient client = new WebClient())
        {
            string html = client.DownloadString(hostname);
            //Debug.Log(html);

            string[] htmlLines = html.Split('\n');
            
            Regex regex = new Regex(expr);


            foreach (string line in htmlLines)
            {
                if (regex.IsMatch(line))
                {
                    MatchCollection mc = Regex.Matches(line, expr);
                    foreach (Match m in mc)
                    {
                        string nextUrl = m.ToString().TrimStart("href=".ToCharArray()).Replace("\"", "");
                        //Debug.Log("Next URL: " + nextUrl);
                        if (nextUrl.StartsWith("http"))
                        {
                            //Debug.Log("URL detected with http");
                            //nextUrl = nextUrl;
                        }
                        else if (nextUrl.StartsWith("./"))
                        {
                            //Debug.Log("URL detected with relative pathing");
                            nextUrl = hostname + nextUrl.TrimStart("./".ToCharArray());
                        }
                        else if (nextUrl.StartsWith("/"))
                        {
                            //Debug.Log("URL detected with absolute pathing");
                            nextUrl = origHostName + nextUrl.TrimStart("/".ToCharArray());
                        }
                        //Ignore all these links
                        else if (nextUrl.StartsWith("mailto"))
                        {
                            //Debug.Log("Ignoring nextUrl: " + nextUrl);
                            nextUrl = null;
                        }
                        else if (nextUrl.EndsWith("css"))
                        {
                            Debug.Log("Ignoring nextUrl: " + nextUrl);
                            nextUrl = null;
                        }
                        else if (nextUrl.EndsWith("jpg"))
                        {
                            Debug.Log("Ignoring nextUrl: " + nextUrl);
                            nextUrl = null;
                        }
                        else
                        {
                            //Debug.Log("URL else detected");
                            nextUrl = hostname + nextUrl;
                        }
                        
                        if (nextUrl != null)
                        {
                            //Debug.Log("Crawled URL: " + nextUrl);
                            if (nextUrl.StartsWith(origHostName))
                            {
                                //Debug.Log("Adding URL: " + nextUrl);
                                urls.Add(nextUrl);
                            }
                            else
                            {
                                //Debug.Log("Outside host detected: " + nextUrl);
                            }
                        }
                     
                    }

                }
            }
            
        }
        return urls;
    }

    
}
