﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CSharpSynth.Effects;
using CSharpSynth.Sequencer;
using CSharpSynth.Synthesis;
using CSharpSynth.Midi;
using System.IO;
using System.Collections;
using System;
using System.Threading;
using System.Linq;


[RequireComponent(typeof(AudioSource))]
public class ReplayerGameFormat : MonoBehaviour
{
    //Public
    //Check the Midi's file folder for different songs
    public string songDirectory = "Songs/KH-Hikari/";
    //Try also: "FM Bank/fm" or "Analog Bank/analog" for some different sounds
    public string bankFilePath = "GM Bank/gm";
    public int bufferSize = 1024;
    public int midiNote = 60;
    public int midiNoteVolume = 100;
    public int midiInstrument = 1;
    //Private 
    private float[] sampleBuffer;
    private float gain = 1f;
    private MidiSequencer midiSequencer;
    private StreamSynthesizer midiStreamSynthesizer;
    private double PitchWheelSemitoneRange = 2.0;
    
    private float sliderValue = 1.0f;
    private float maxSliderValue = 127.0f;

    string[] songFiles;
    Track track;
    List<Track> tracks = new List<Track>();
    List<int> playIndex = new List<int>();

    public List<int> instrumentsInSong = new List<int>();

    bool _threadRunning;
    Thread _thread;
    bool workDone;
    public float progress = 0f;
    LoadingBarController loadbar;

    bool playing = false;

    public bool debugTrackLoad = false;

    public HitMarkerSpawner spawner; 

    // Awake is called when the script instance
    // is being loaded.
    void Awake()
    {

        //Load game components
        loadbar = GetComponent<LoadingBarController>();
        if (loadbar == null)
        {
            throw new Exception("MidiEditor Requires a Loading Bar");
        }


        midiStreamSynthesizer = new StreamSynthesizer(44100, 2, bufferSize, 40);
        sampleBuffer = new float[midiStreamSynthesizer.BufferSize];

        midiStreamSynthesizer.LoadBank(bankFilePath);

        midiSequencer = new MidiSequencer(midiStreamSynthesizer);
        //init();
        
    }

    public void init()
    {
        if (!_threadRunning)
        {
            Debug.Log("Song is loading in background");
            _thread = new Thread(ThreadedWork);
            _thread.Start();
        }
        
    }

    public MidiEditor editor;
    void ThreadedWork()
    {
        _threadRunning = true;
        workDone = false;

        
        // This pattern lets us interrupt the work at a safe point if neeeded.
        while (_threadRunning && !workDone)
        {
            // Do Work...

            /*songFiles = getSongFileList(songDirectory);
            for (int i = 0; i < songFiles.Length; i++)
            {
                ParseSongToMemory(songFiles[i]);
                progress = (float)i / (float)songFiles.Length;
                //Debug.Log(progress.ToString("F4"));
            }*/
            
            if (editor != null)
            {
                ParseSongToMemory(editor.outputTracks);
            }
            else
            {
                throw new Exception("No Editor found in scene");
            }

            progress = 1f;
            killThread();
        }
        _threadRunning = false;
    }

    private void killThread()
    {
        Debug.Log("Background thread complete");
        workDone = true; //kill background thread on editor stop
        _threadRunning = false;
    }

    void OnDisable()
    {
        // If the thread is still running, we should shut it down,
        // otherwise it can prevent the game from exiting correctly.
        if (_threadRunning)
        {
            // This forces the while loop in the ThreadedWork function to abort.
            _threadRunning = false;

            // This waits until the thread exits,
            // ensuring any cleanup we do after this is safe. 
            _thread.Join();
        }

        // Thread is guaranteed no longer running. Do other cleanup tasks.
    }

    /*void ParseSongToMemory(string songfile)
    {
        track = new Track(songfile);
        tracks.Add(track);
        Debug.Log("Adding track");
        playIndex.Add(0);
    }*/

    void ParseSongToMemory(List<List<string>> songData)
    {
        HashSet<int> instruments = new HashSet<int>();
        for (int i = 0; i < songData.Count; i++)
        {
            track = new Track(songData[i]);
            tracks.Add(track);

            if (debugTrackLoad)
                Debug.Log("Adding track");
            playIndex.Add(0);

            instruments.UnionWith(track.instrumentsInTrack);
            
            
        }
        instrumentsInSong = instruments.ToList<int>();
        instrumentsInSong.Sort();

        spawner.availableInstruments = instrumentsInSong;
    }

    public bool muteReplayer = false;
    void Update()
    {
        try
        {

            loadbar.SetProgress(progress);

            if (playing)
            {
                if (muteReplayer)
                    midiStreamSynthesizer.NoteOffAll(true);

                //Debug.Log((Time.time-startTime).ToString());

                for (int i = 0; i < tracks.Count; i++)
                {
                    for (int e = playIndex[i]; e < tracks[i].notes.Count; e++)
                    {
                        float time = tracks[i].notes[e].time;
                        if (time <= (Time.time - startTime))
                        {
                            int type = tracks[i].notes[e].type;
                            if (type == (int)NoteEvent.eventType.NoteOn)
                            {
                                int note = tracks[i].notes[e].parameter1;
                                int volume = tracks[i].notes[e].parameter2;
                                int channel = tracks[i].notes[e].channel;
                                int instrument = tracks[i].notes[e].program;
                                spawner.addQueue(note, instrument);
                                //Debug.Log(instrument);
                                if (!muteReplayer)
                                    midiStreamSynthesizer.NoteOn(channel, note, volume, instrument);
                            }
                            else if (type == (int)NoteEvent.eventType.NoteOff)
                            {
                                int note = tracks[i].notes[e].parameter1;
                                int channel = tracks[i].notes[e].channel;
                                if (!muteReplayer)
                                    midiStreamSynthesizer.NoteOff(channel, note);
                            }
                            else if (type == (int)NoteEvent.eventType.Pitch_Bend)
                            {
                                int parameter1 = tracks[i].notes[e].parameter1;
                                int parameter2 = tracks[i].notes[e].parameter2;
                                int channel = tracks[i].notes[e].channel;

                                ushort s = (ushort)parameter1;
                                s <<= 7;
                                s |= (ushort)parameter2;
                                double bend = ((double)s - 8192.0) / 8192.0;

                                midiStreamSynthesizer.TunePositions[channel] = (double)bend * PitchWheelSemitoneRange;
                            }
                            else if (type == (int)NoteEvent.eventType.Controller)
                            {
                                int parameter1 = tracks[i].notes[e].parameter1;
                                int parameter2 = tracks[i].notes[e].parameter2;
                                int channel = tracks[i].notes[e].channel;

                                switch (GetControllerType(parameter1))
                                {
                                    case MidiHelper.ControllerType.AllNotesOff:
                                        midiStreamSynthesizer.NoteOffAll(true);
                                        break;
                                    case MidiHelper.ControllerType.MainVolume:
                                        midiStreamSynthesizer.VolPositions[channel] = parameter2 / 127.0f;
                                        break;
                                    case MidiHelper.ControllerType.Pan:
                                        midiStreamSynthesizer.PanPositions[channel] = (parameter2 - 64) == 63 ? 1.00f : (parameter2 - 64) / 64.0f;
                                        break;
                                    case MidiHelper.ControllerType.ResetControllers:
                                        ResetControllers();
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        else
                        {
                            playIndex[i] = e;
                            break;
                        }
                    }
                }
            }
        
        }
        catch (Exception e) 
        {
            Debug.Log(e.StackTrace);
            Debug.Log(e.Message);
        }
    }

    public void ResetControllers()
    {
        //Reset Pan Positions back to 0.0f
        Array.Clear(midiStreamSynthesizer.PanPositions, 0, midiStreamSynthesizer.PanPositions.Length);
        //Set Tuning Positions back to 0.0f
        Array.Clear(midiStreamSynthesizer.TunePositions, 0, midiStreamSynthesizer.TunePositions.Length);
        //Reset Vol Positions back to 1.00f
        for (int x = 0; x < midiStreamSynthesizer.VolPositions.Length; x++)
            midiStreamSynthesizer.VolPositions[x] = 1.00f;
    }

    public MidiHelper.ControllerType GetControllerType(int parameter1)
    {
        switch (parameter1)
        {
            case 1:
                return MidiHelper.ControllerType.Modulation;
            case 7:
                return MidiHelper.ControllerType.MainVolume;
            case 10:
                return MidiHelper.ControllerType.Pan;
            case 64:
                return MidiHelper.ControllerType.DamperPedal;
            case 121:
                return MidiHelper.ControllerType.ResetControllers;
            case 123:
                return MidiHelper.ControllerType.AllNotesOff;
            default:
                return MidiHelper.ControllerType.Unknown;
        }
    }

    string[] getSongFileList(string targetDirectory)
    {
        string[] fileEntries = Directory.GetFiles(Directory.GetCurrentDirectory() + "\\" + targetDirectory, "*.txt");

        return fileEntries;
    }
    
    
    // OnGUI is called for rendering and handling
    // GUI events.
    void OnGUI()
    {
        GUILayout.BeginArea(new Rect(10, Screen.height / 2 - 50, 150, 300));

        if (GUILayout.Button("Play Song"))
        {
            PlaySong();
        }
        if (GUILayout.Button("Stop Song"))
        {
            StopSong();
        }
        if (GUILayout.Button("Load Game Formatted Song"))
        {
            init();
        }
        if (GUILayout.Button("DebugLog the notes"))
        {
            for(int i = 0; i < tracks.Count; i++)
            {
                Debug.Log("NEW TRACK: " + i.ToString() + " has " + tracks[i].notes.Count + " notes");
                /*for(int j = 0; j < tracks[i].notes.Count; j++)
                {
                    NoteEvent e = tracks[i].notes[j];
                    Debug.Log(e.channel + "," + e.program + ", " + e.parameter1 + "," + e.parameter2 + "," + e.time);
                }*/
            }
        }
        GUILayout.EndArea();
    }

    float startTime = 0f;
    public void PlaySong()
    {
        if (playing)
        {
            Debug.Log("Song is already playing!");
        }
        else
        {
            startTime = Time.time;
            playing = true;
            Debug.Log("Play Song");
        }
        

        //midiStreamSynthesizer.NoteOn(1, midiNote + 12, midiNoteVolume, midiInstrument);
    }

    void StopSong()
    {
        playing = false;
        Debug.Log("Stop Song");
        midiStreamSynthesizer.NoteOffAll(true);
        for (int i = 0; i < playIndex.Count; i++)
        {
            playIndex[i] = 0;
        }
    }

    private void OnAudioFilterRead(float[] data, int channels)
    {
        if (midiStreamSynthesizer != null)
        {
            try
            {
                midiStreamSynthesizer.GetNext(sampleBuffer);
                for (int i = 0; i < data.Length; i++)
                {
                    data[i] = sampleBuffer[i] * gain;
                }
            }
            catch (Exception e)
            {

            }
            

        }
            

    }

}
