﻿using UnityEngine;
using System.Collections;

public class NoteInfo 
{
    string type = ""; //NoteOn, NoteOff
    int channel; //midiEvent.channel
    int note; //parameter1
    int volume; //parameter2
    int instrument; //currentPrograms[midiEvent.channel]
    public bool mute;

    public NoteInfo(int channel, int note, int volume, int instrument, string type)
    {
        this.channel = channel;
        this.note = note;
        this.volume = volume;
        this.instrument = instrument;
        this.type = type;
        this.mute = false;
    }

    public int getChannel()
    {
        return channel;
    }

    public int getNote()
    {
        return note;
    }

    public int getVolume()
    {
        return volume;
    }

    public int getInstrument()
    {
        return instrument;
    }
}
