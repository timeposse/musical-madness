﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using CSharpSynth.Midi;

public class Track {
    private int activeProgram;
    bool verboseNoteLoad = false;
    bool veryVerboseNoteLoad = false;


    public HashSet<int> instrumentsInTrack = new HashSet<int>();
    public List<NoteEvent> notes = new List<NoteEvent>();

    public Track(List<string> data)
    {
        if (verboseNoteLoad)
            Debug.Log("Data Contains " + data.Count.ToString() + " events");
        parseLines(data);
            
    }

    private void parseLines(List<string> lines)
    {
        
        for (int i = 0; i < lines.Count; i++)
        {
            if (veryVerboseNoteLoad)
                Debug.Log(lines[i]);

            string[] data = lines[i].Split(',');
            if (data[0] == "Program_Change")
            {
                int program, channel;
                float time;

                int type = (int)NoteEvent.eventType.Program_Change;
                int.TryParse(data[1], out channel);
                int.TryParse(data[2], out program);
                float.TryParse(data[4], out time);
                int parameter1 = 0;
                int parameter2 = 0;
                activeProgram = program;

                NoteEvent newNote = new NoteEvent(type, channel, parameter1, parameter2, program, time);
                notes.Add(newNote);
                instrumentsInTrack.Add(program);
            }
            else if (data[0] == "Pitch_Bend")
            {
                int program, channel, parameter1, parameter2;
                float time;

                int type = (int)NoteEvent.eventType.Pitch_Bend;
                int.TryParse(data[1], out channel);
                int.TryParse(data[2], out parameter1);
                int.TryParse(data[3], out parameter2);
                float.TryParse(data[4], out time);
                program = activeProgram;

                NoteEvent newNote = new NoteEvent(type, channel, parameter1, parameter2, program, time);
                notes.Add(newNote);
            }
            else if (data[0] == "Controller")
            {
                int program, channel, parameter1, parameter2;
                float time;

                int type = (int)NoteEvent.eventType.Controller;
                int.TryParse(data[1], out channel);
                int.TryParse(data[2], out parameter1);
                int.TryParse(data[3], out parameter2);
                float.TryParse(data[4], out time);
                program = activeProgram;

                NoteEvent newNote = new NoteEvent(type, channel, parameter1, parameter2, program, time);
                notes.Add(newNote);
            }
            else if (data[0] == "Note_On")
            {
                int type, channel, parameter1, parameter2, program;
                float time;
                type = (int)NoteEvent.eventType.NoteOn;
                int.TryParse(data[1], out channel);
                int.TryParse(data[2], out parameter1);
                int.TryParse(data[3], out parameter2);

                program = activeProgram;
                float.TryParse(data[4], out time);

                NoteEvent newNote = new NoteEvent(type, channel, parameter1, parameter2, program, time);
                notes.Add(newNote);
            }
            else if (data[0] == "Note_Off")
            {
                int type, channel, parameter1, parameter2, program;
                float time;

                type = (int)NoteEvent.eventType.NoteOff;
                int.TryParse(data[1], out channel);
                int.TryParse(data[2], out parameter1);
                int.TryParse(data[3], out parameter2);
                program = activeProgram;
                float.TryParse(data[4], out time);

                NoteEvent newNote = new NoteEvent(type, channel, parameter1, parameter2, program, time);
                notes.Add(newNote);
            }
        }
        //Debug.Log("Notes size: " + notes.Count.ToString());
    }

}
