﻿using UnityEngine;
using System.Collections;

public class Falling : MonoBehaviour {
    public float fallSpeed = 0.5f;
	
	void Update () {
        Vector3 position = transform.position;
        position.y -= fallSpeed * Time.deltaTime;
        transform.position = position;
	}
}
