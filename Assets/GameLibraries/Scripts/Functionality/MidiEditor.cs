﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using UnityEngine;
using CSharpSynth.Midi;
using System.Threading; 
using UnityEditor;
using CSharpSynth.Synthesis;
using CSharpSynth.Banks;

public class MidiEditor : MonoBehaviour {
    private MidiTrack[] tracks;
    private MidiHeader mheader;
    public String midifile;
    uint BeatsPerMinute = 120;

    //Vars for loading and parsing midi files in second thread
    public bool convertToGameFormat = true;

    bool isConverting = false;
    bool _threadRunning;
    Thread _thread;
    bool workDone;
    public float progress = 0f;
    LoadingBarController loadbar;


    void Awake()
    {
        //Load game components
        loadbar = GetComponent<LoadingBarController>();
        if (loadbar == null)
        {
            throw new Exception("MidiEditor Requires a Loading Bar");
        }
        //ConvertMidiToGameFormat();

    }

    void OnGUI()
    {
        // Make a background box
        GUILayout.BeginArea(new Rect(10, Screen.height / 2 - 80, 150, 300));


        if (GUILayout.Button("Convert Song"))
        {
            ConvertMidiToGameFormat();
        }

        GUILayout.EndArea();

    }

    public void ConvertMidiToGameFormat()
    {
        if (!isConverting)
        {
            isConverting = true;

            LoadFile(midifile);
            
            // Begin our heavy work on a new thread.
            _thread = new Thread(ThreadedWork);
            _thread.Start();
        }
        else
        {
            Debug.Log("Conversion already in progress");
        }
    }

    void Update()
    {
        if (!EditorApplication.isPlayingOrWillChangePlaymode)
        {
            killThread();
        }

        loadbar.SetProgress(progress);
    }

    private void killThread()
    {
        Debug.Log("Killing background conversion thread");
        workDone = true; //kill background thread on editor stop
        _threadRunning = false;
        isConverting = false;
    }

    

    void ThreadedWork()
    {
        _threadRunning = true;
        workDone = false;

        // This pattern lets us interrupt the work at a safe point if neeeded.
        while (_threadRunning && !workDone)
        {
            // Do Work...
            //FoxHelper.ClearTempSpace("DevTemp");
            ConvertMidi();
            killThread();
        }
        _threadRunning = false;
    }

    void OnDisable()
    {
        // If the thread is still running, we should shut it down,
        // otherwise it can prevent the game from exiting correctly.
        if (_threadRunning)
        {
            // This forces the while loop in the ThreadedWork function to abort.
            _threadRunning = false;

            // This waits until the thread exits,
            // ensuring any cleanup we do after this is safe. 
            _thread.Join();
        }

        // Thread is guaranteed no longer running. Do other cleanup tasks.
    }

    void LoadFile(string filename)
        {
            Stream midiStream = null;
            try
            {
                string directory = Directory.GetCurrentDirectory() + "/";
                //UnitySynth
                //midiStream = File.Open(filename, FileMode.Open);
                Debug.Log("Loading midi: " + directory + filename);
                //TextAsset midiFileName = Resources.LoadAsync(directory + filename).asset as TextAsset;
                byte[] bytes = File.ReadAllBytes(directory + filename); 
                midiStream = new MemoryStream(bytes);
                loadStream(midiStream);
                Debug.Log("Loaded " + midifile);
            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message);
                throw new Exception("Midi Failed to Load!", ex);
            }
            finally
            {
                if (midiStream != null)
                    midiStream.Close();
            }
        }

    public List< List<string> > outputTracks = new List<List<string> >();
    public bool writeFile = false;
    public void ConvertMidi()
    {
        if (tracks.Length < 2)
            return;
        int total_eventCount = 0;
        UInt64 total_notesPlayed = 0;
        List<byte> programsUsed = new List<byte>();
        List<byte> DrumprogramsUsed = new List<byte>();
        
        //Loop to get track info
        //writeToFile("midiexport.txt", "Total Tracks: " + tracks.Length.ToString());
        float maxprogress = tracks.Length;

        for (int x = 0; x < tracks.Length; x++)
        {
            if (workDone)
                return;

            List<string> outputTrack = new List<string>();
            outputTrack.Clear();
            double time = 0.0;
            for (int e = 0; e < tracks[x].EventCount; e++)
            {
                if (workDone)
                    return;

                //lastSample = tracks[x].MidiEvents[e].deltaTime;
                //Update tempo
                if (tracks[x].MidiEvents[e].midiMetaEvent == MidiHelper.MidiMetaEvent.Tempo)
                {
                    BeatsPerMinute = MidiHelper.MicroSecondsPerMinute / System.Convert.ToUInt32(tracks[x].MidiEvents[e].Parameters[0]);
                }
                double deltaMS = tracks[x].MidiEvents[e].deltaTime * (1000.0 * (60.0 / (BeatsPerMinute * mheader.DeltaTiming)));

                time += deltaMS;

                string eventInfo = tracks[x].MidiEvents[e].ToString(convertToGameFormat);
                if (eventInfo != null)
                {
                    if (convertToGameFormat)
                    {
                        string data = eventInfo + "," + (time / 1000.0).ToString();
                        outputTrack.Add(data);
                        if (writeFile)
                            writeToFile("midiexport_track_" + x.ToString() + ".txt", data);
                    }
                    else
                    {
                        string data = eventInfo;
                        outputTrack.Add(data);
                        if (writeFile)
                            writeToFile("midiexport_track_" + x.ToString() + ".txt", data);
                    }
                }


                progress = ((float)x + ((float)e / (float)tracks[x].EventCount)) / (float)tracks.Length; //events/totalEvents per track
            }
            outputTracks.Add(outputTrack);

        }

        progress = 1f;
        Debug.Log("Tracks converted: " + outputTracks.Count.ToString());
        //writeToFile("midiexport.txt", "Total Events: " + total_eventCount.ToString());
        //writeToFile("midiexport.txt", "Total Notes Played: " + total_notesPlayed.ToString());
    }

    public void writeToFile(string filename, string lines)
    {
        string dir = "DevTemp/";
        System.IO.StreamWriter file = null;
        try
        {
            // Write the string to a file.
            file = new System.IO.StreamWriter(dir + filename, true);
            file.WriteLine(lines);

        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
            throw ex;
        }
        finally
        {
            if (file != null)
                file.Close();
        }
        
    }

    private void loadStream(Stream stream)
    {
        byte[] tmp = new byte[4];
        stream.Read(tmp, 0, 4);
        if (UTF8Encoding.UTF8.GetString(tmp, 0, tmp.Length) != "MThd")
            throw new Exception("Not a valid midi file!");
        mheader = new MidiHeader();
        //Read header length
        stream.Read(tmp, 0, 4);
        Array.Reverse(tmp); //Reverse the bytes
        int headerLength = BitConverter.ToInt32(tmp, 0);
        //Read midi format
        tmp = new byte[2];
        stream.Read(tmp, 0, 2);
        Array.Reverse(tmp); //Reverse the bytes
        mheader.setMidiFormat(BitConverter.ToInt16(tmp, 0));
        //Read Track Count
        stream.Read(tmp, 0, 2);
        Array.Reverse(tmp); //Reverse the bytes
        int trackCount = BitConverter.ToInt16(tmp, 0);
        tracks = new MidiTrack[trackCount];
        //Read Delta Time
        stream.Read(tmp, 0, 2);
        Array.Reverse(tmp); //Reverse the bytes
        int delta = BitConverter.ToInt16(tmp, 0);
        mheader.DeltaTiming = (delta & 0x7FFF);
        //Time Format
        mheader.TimeFormat = ((delta & 0x8000) > 0) ? MidiHelper.MidiTimeFormat.FamesPerSecond : MidiHelper.MidiTimeFormat.TicksPerBeat;
        //Begin Reading Each Track
        for (int x = 0; x < trackCount; x++)
        {
            List<byte> Programs = new List<byte>();
            List<byte> DrumPrograms = new List<byte>();
            List<MidiEvent> midiEvList = new List<MidiEvent>();
            tracks[x] = new MidiTrack();
            Programs.Add(0); //assume the track uses program at 0 in case no program changes are used
            DrumPrograms.Add(0);
            tmp = new byte[4];      //reset the size again
            stream.Read(tmp, 0, 4);
            if (UTF8Encoding.UTF8.GetString(tmp, 0, tmp.Length) != "MTrk")
                throw new Exception("Invalid track!");
            stream.Read(tmp, 0, 4);
            Array.Reverse(tmp); //Reverse the bytes
            int TrackLength = BitConverter.ToInt32(tmp, 0);
            //Read The Rest of The Track
            tmp = new byte[TrackLength];
            stream.Read(tmp, 0, TrackLength);
            int index = 0;
            byte prevByte = 0;
            int prevChan = 0;
            while (index < tmp.Length)
            {
                UInt16 numofbytes = 0;
                UInt32 ScrmbledDta = BitConverter.ToUInt32(tmp, index);
                MidiEvent MEv = new MidiEvent();
                MEv.deltaTime = GetTime(ScrmbledDta, ref numofbytes);
                index += 4 - (4 - numofbytes);
                byte statusByte = tmp[index];
                int CHANNEL = GetChannel(statusByte);
                if (statusByte < 0x80)
                {
                    statusByte = prevByte;
                    CHANNEL = prevChan;
                    index--;
                }
                if (statusByte != 0xFF)
                    statusByte &= 0xF0;
                prevByte = statusByte;
                prevChan = CHANNEL;
                switch (statusByte)
                {
                    case 0x80:
                        {
                            MEv.midiChannelEvent = MidiHelper.MidiChannelEvent.Note_Off;
                            ++index;
                            MEv.channel = (byte)CHANNEL;
                            MEv.Parameters[0] = MEv.channel;
                            MEv.parameter1 = tmp[index++];
                            MEv.parameter2 = tmp[index++];
                            MEv.Parameters[1] = MEv.parameter1;
                            MEv.Parameters[2] = MEv.parameter2;
                        }
                        break;
                    case 0x90:
                        {
                            MEv.midiChannelEvent = MidiHelper.MidiChannelEvent.Note_On;
                            ++index;
                            MEv.channel = (byte)CHANNEL;
                            MEv.Parameters[0] = MEv.channel;
                            MEv.parameter1 = tmp[index++];
                            MEv.parameter2 = tmp[index++];
                            MEv.Parameters[1] = MEv.parameter1;
                            MEv.Parameters[2] = MEv.parameter2;
                            if (MEv.parameter2 == 0x00) //Setting velocity to 0 is actually just turning the note off.
                                MEv.midiChannelEvent = MidiHelper.MidiChannelEvent.Note_Off;
                            tracks[x].NotesPlayed++;
                        }
                        break;
                    case 0xA0:
                        {
                            MEv.midiChannelEvent = MidiHelper.MidiChannelEvent.Note_Aftertouch;
                            MEv.channel = (byte)CHANNEL;
                            MEv.Parameters[0] = MEv.channel;
                            ++index;
                            MEv.parameter1 = tmp[++index];//note number
                            MEv.parameter2 = tmp[++index];//Amount
                        }
                        break;
                    case 0xB0:
                        {
                            MEv.midiChannelEvent = MidiHelper.MidiChannelEvent.Controller;
                            MEv.channel = (byte)CHANNEL;
                            MEv.Parameters[0] = MEv.channel;
                            ++index;
                            MEv.parameter1 = tmp[index++]; //type
                            MEv.parameter2 = tmp[index++]; //value
                            MEv.Parameters[1] = MEv.parameter1;
                            MEv.Parameters[2] = MEv.parameter2;
                        }
                        break;
                    case 0xC0:
                        {
                            MEv.midiChannelEvent = MidiHelper.MidiChannelEvent.Program_Change;
                            MEv.channel = (byte)CHANNEL;
                            MEv.Parameters[0] = MEv.channel;
                            ++index;
                            MEv.parameter1 = tmp[index++];
                            MEv.Parameters[1] = MEv.parameter1;
                            //record which programs are used by the track
                            if (MEv.channel != 9)
                            {
                                if (Programs.Contains(MEv.parameter1) == false)
                                    Programs.Add(MEv.parameter1);
                            }
                            else
                            {
                                if (DrumPrograms.Contains(MEv.parameter1) == false)
                                    DrumPrograms.Add(MEv.parameter1);
                            }
                        }
                        break;
                    case 0xD0:
                        {
                            MEv.midiChannelEvent = MidiHelper.MidiChannelEvent.Channel_Aftertouch;
                            MEv.channel = (byte)CHANNEL;
                            MEv.Parameters[0] = MEv.channel;
                            ++index;
                            //Amount
                            MEv.parameter1 = tmp[++index];
                        }
                        break;
                    case 0xE0:
                        {
                            MEv.midiChannelEvent = MidiHelper.MidiChannelEvent.Pitch_Bend;
                            MEv.channel = (byte)CHANNEL;
                            MEv.Parameters[0] = MEv.channel;
                            ++index;
                            MEv.parameter1 = tmp[++index];
                            MEv.parameter2 = tmp[++index];
                            ushort s = (ushort)MEv.parameter1;
                            s <<= 7;
                            s |= (ushort)MEv.parameter2;
                            MEv.Parameters[1] = ((double)s - 8192.0) / 8192.0;
                        }
                        break;
                    case 0xFF:
                        statusByte = tmp[++index];
                        switch (statusByte)
                        {
                            case 0x00:
                                MEv.midiMetaEvent = MidiHelper.MidiMetaEvent.Sequence_Number; ++index;
                                break;
                            case 0x01:
                                MEv.midiMetaEvent = MidiHelper.MidiMetaEvent.Text_Event; ++index;
                                //Get the length of the string
                                MEv.parameter1 = tmp[index++];
                                MEv.Parameters[0] = MEv.parameter1;
                                //Set the string in the parameter list
                                MEv.Parameters[1] = UTF8Encoding.UTF8.GetString(tmp, index, ((int)tmp[index - 1])); index += (int)tmp[index - 1];
                                break;
                            case 0x02:
                                MEv.midiMetaEvent = MidiHelper.MidiMetaEvent.Copyright_Notice; ++index;
                                //Get the length of the string
                                MEv.parameter1 = tmp[index++];
                                MEv.Parameters[0] = MEv.parameter1;
                                //Set the string in the parameter list
                                MEv.Parameters[1] = UTF8Encoding.UTF8.GetString(tmp, index, ((int)tmp[index - 1])); index += (int)tmp[index - 1];
                                break;
                            case 0x03:
                                MEv.midiMetaEvent = MidiHelper.MidiMetaEvent.Sequence_Or_Track_Name; ++index;
                                //Get the length of the string
                                MEv.parameter1 = tmp[index++];
                                MEv.Parameters[0] = MEv.parameter1;
                                //Set the string in the parameter list
                                MEv.Parameters[1] = UTF8Encoding.UTF8.GetString(tmp, index, ((int)tmp[index - 1])); index += (int)tmp[index - 1];
                                break;
                            case 0x04:
                                MEv.midiMetaEvent = MidiHelper.MidiMetaEvent.Instrument_Name; ++index;
                                //Set the instrument name
                                MEv.Parameters[0] = UTF8Encoding.UTF8.GetString(tmp, index + 1, (int)tmp[index]);
                                index += (int)tmp[index] + 1;
                                break;
                            case 0x05:
                                MEv.midiMetaEvent = MidiHelper.MidiMetaEvent.Lyric_Text; ++index;
                                //Set the lyric string
                                MEv.Parameters[0] = UTF8Encoding.UTF8.GetString(tmp, index + 1, (int)tmp[index]);
                                index += (int)tmp[index] + 1;
                                break;
                            case 0x06:
                                MEv.midiMetaEvent = MidiHelper.MidiMetaEvent.Marker_Text; ++index;
                                //Set the marker
                                MEv.Parameters[0] = UTF8Encoding.UTF8.GetString(tmp, index + 1, (int)tmp[index]);
                                index += (int)tmp[index] + 1;
                                break;
                            case 0x07:
                                MEv.midiMetaEvent = MidiHelper.MidiMetaEvent.Cue_Point; ++index;
                                //Set the cue point
                                MEv.Parameters[0] = UTF8Encoding.UTF8.GetString(tmp, index + 1, (int)tmp[index]);
                                index += (int)tmp[index] + 1;
                                break;
                            case 0x20:
                                MEv.midiMetaEvent = MidiHelper.MidiMetaEvent.Midi_Channel_Prefix_Assignment; index++;
                                //Get the length of the data
                                MEv.parameter1 = tmp[index++];
                                MEv.Parameters[0] = MEv.parameter1;
                                //Set the string in the parameter list
                                MEv.Parameters[1] = tmp[index++];
                                break;
                            case 0x2F:
                                MEv.midiMetaEvent = MidiHelper.MidiMetaEvent.End_of_Track;
                                index += 2;
                                break;
                            case 0x51:
                                MEv.midiMetaEvent = MidiHelper.MidiMetaEvent.Tempo; ++index;
                                //Get the length of the data
                                MEv.Parameters[4] = tmp[index++];
                                //Put the data into an array
                                byte[] mS = new byte[4]; for (int i = 0; i < 3; i++) mS[i + 1] = tmp[i + index]; index += 3;
                                //Put it into a readable format
                                byte[] mS2 = new byte[4]; for (int i = 0; i < 4; i++) mS2[3 - i] = mS[i];
                                //Get the value from the array
                                UInt32 Val = BitConverter.ToUInt32(mS2, 0);
                                //Set the value
                                MEv.Parameters[0] = Val;
                                break;
                            case 0x54:
                                MEv.midiMetaEvent = MidiHelper.MidiMetaEvent.Smpte_Offset; ++index;
                                int v = tmp[index++];
                                if (v >= 4)
                                    for (int i = 0; i < 4; i++) MEv.Parameters[i] = tmp[index++];
                                else
                                    for (int i = 0; i < v; i++) MEv.Parameters[i] = tmp[index++];
                                for (int i = 4; i < v; i++) index++;
                                break;
                            case 0x58:
                                MEv.midiMetaEvent = MidiHelper.MidiMetaEvent.Time_Signature; ++index;
                                int v1 = tmp[index++];
                                if (v1 >= 4)
                                    for (int i = 0; i < 4; i++) MEv.Parameters[i] = tmp[index++];
                                else
                                    for (int i = 0; i < v1; i++) MEv.Parameters[i] = tmp[index++];
                                for (int i = 4; i < v1; i++) index++;
                                break;
                            case 0x59:
                                MEv.midiMetaEvent = MidiHelper.MidiMetaEvent.Key_Signature; ++index;
                                int v2 = tmp[index++];
                                if (v2 >= 4)
                                    for (int i = 0; i < 4; i++) MEv.Parameters[i] = tmp[index++];
                                else
                                    for (int i = 0; i < v2; i++) MEv.Parameters[i] = tmp[index++];
                                for (int i = 4; i < v2; i++) index++;
                                break;
                            case 0x7F:
                                //Sequencer specific events
                                MEv.midiMetaEvent = MidiHelper.MidiMetaEvent.Sequencer_Specific_Event; ++index;    //increment the indexer
                                //Get the length of the data
                                MEv.Parameters[4] = tmp[index++];
                                //Get the byte length
                                byte[] len = new byte[(byte)MEv.Parameters[4]];
                                //get the byte info
                                for (int i = 0; i < len.Length; i++) len[i] = tmp[index++];
                                MEv.Parameters[0] = len;
                                break;
                        }
                        break;
                    //System exclusive
                    case 0xF0:
                        while (tmp[index] != 0xF7)
                            index++;
                        index++;
                        break;
                }
                midiEvList.Add(MEv);
                tracks[x].TotalTime = tracks[x].TotalTime + MEv.deltaTime;
            }
            tracks[x].Programs = Programs.ToArray();
            tracks[x].DrumPrograms = DrumPrograms.ToArray();
            tracks[x].MidiEvents = midiEvList.ToArray();
        }
    }

    private int GetChannel(byte statusbyte)
    {
        statusbyte = (byte)(statusbyte << 4);
        return statusbyte >> 4;
    }
    private uint GetTime(UInt32 data, ref UInt16 numOfBytes)
    {
        byte[] buff = BitConverter.GetBytes(data); numOfBytes++;
        for (int i = 0; i < buff.Length; i++) { if ((buff[i] & 0x80) > 0) { numOfBytes++; } else { break; } }
        for (int i = numOfBytes; i < 4; i++) buff[i] = 0x00;
        Array.Reverse(buff);
        data = BitConverter.ToUInt32(buff, 0);
        data >>= (32 - (numOfBytes * 8));
        UInt32 b = data;
        UInt32 bffr = (data & 0x7F);
        int c = 1;
        while ((data >>= 8) > 0)
        {
            bffr |= ((data & 0x7F) << (7 * c)); c++;
        }
        return bffr;
    }


}
