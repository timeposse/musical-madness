﻿using UnityEngine;
using System.Collections;

public class SongTitle : MonoBehaviour {
    public static string title = "tmp";
    UIInputSaved uiInput;
    bool selected = false;

    void Awake()
    {
        string label = GetComponent<UILabel>().text;
        GetComponent<UIInputSaved>().text = label;
        uiInput = GetComponent<UIInputSaved>();
    }

    void Update()
    {
        if (uiInput != null)
        {
            selected = uiInput.selected;
        }
        if (selected)
        {
            InputController.disabled = true;
        }
        else
        {
            InputController.disabled = false;
        }
    }

    public void OnSubmit()
    {
        string label = GetComponent<UILabel>().text;
        label = label.Remove(label.Length - 1);
        GetComponent<UIInputSaved>().text = label;
        SongTitle.title = label;
        
        Debug.Log("SongTitle: " + SongTitle.title);
    }


}
