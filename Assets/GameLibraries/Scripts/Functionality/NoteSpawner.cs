﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class NoteSpawner : MonoBehaviour {
    public GameObject fallingNote;
    public GameObject fallingNoteOff;
    public Transform[] spawnPoints;
    List<NoteInfo> notesToSpawn = new List<NoteInfo>();
    List<int> notesOffToSpawn = new List<int>();
    int spawnIndex = 0;
    int offset = 3;
    public GameObject lastNoteSpawned;
    NoteText lastText;
    public float combineThreshold = 0.0625f;


    float timeSinceLastNote = 0f;

    public void resetSpawnIndex()
    {
        spawnIndex = 0;
    }

    void Start()
    {
        timeSinceLastNote = 0f;
        spawnIndex = 0;
    }

    public void spawnNote(List<NoteInfo> notesOn)
    {
        for (int i = 0; i < notesOn.Count; i++ )
        {
            notesToSpawn.Add(notesOn[i]);
        }
            
    }

    public void spawnNote(int channel, int note, int volume, int instrument)
    {
        NoteInfo newnote = new NoteInfo(channel, note, volume, instrument, "NoteOn");
        notesToSpawn.Add(newnote);
     }

    public void spawnNoteOff(List<int> incomingNotes)
    {
        notesOffToSpawn = incomingNotes;

    }
    
    void Update()
    {
        string final = "";
        for (int i = 0; i < notesToSpawn.Count; i++)
        {
            string letter = KeyNoteMap.ConvertKeyIndexToLetter(notesToSpawn[i].getNote() - 37);
            final += letter;
        }
        if (final != "")
        {
            if (lastNoteSpawned != null)
                lastText = lastNoteSpawned.GetComponentInChildren<NoteText>();

            if (final.Length > 5)
            {
                int notesNeeded = (int)(((final.Length + 5 ) / 5f));
                //Debug.Log("Notes needed:" + notesNeeded.ToString() + "... Length: " + final.Length.ToString());
                for (int i = 0; i < notesNeeded; i ++ )
                {
                    if (i == 0)
                    {
                        InstantiateNote(final.Substring(0, 5));
                    }
                    else if (i == 1)
                    {
                        InstantiateNote(final.Substring(5));
                    }
                }   
            }
            else if ((lastText == null) || ((Time.time - timeSinceLastNote) > combineThreshold) || ((lastText.getText().Length + final.Length) >= 5))
            {
                InstantiateNote(final);
            }
            else
            {
                ExtendOldNote(final);
            }
        }

        notesToSpawn.Clear();
    }

    private void ExtendOldNote(string final)
    {
        if (lastNoteSpawned != null)
        {
            NoteText noteText = lastNoteSpawned.GetComponentInChildren<NoteText>();
            if (noteText != null)
            {
                //Debug.Log("Changing Note");
                string newText = noteText.getText();
                for (int i = 0; i < final.Length; i++ )
                {
                    char letter = final[i];
                    string supposedNewText = newText + letter;
                    int distinctCount = supposedNewText.Distinct().Count();
                    int count = supposedNewText.Count();
                    if (distinctCount == count)
                    {
                        newText += letter;
                    }
                }
                newText = new string(newText.OrderBy(c => c).ToArray());

                noteText.setText(newText);

            }
            else
            {
                Debug.Log("NoteText not found!");
            }
        }
    }


    private void InstantiateNote(string final)
    {
        //Debug.Log("Spawning note");
        GameObject obj = (GameObject)Instantiate(fallingNote, spawnPoints[spawnIndex].position, spawnPoints[spawnIndex].rotation);
        lastNoteSpawned = obj;
        obj.GetComponentInChildren<NoteText>().setText(final);
        advanceSpawnIndex();
        timeSinceLastNote = Time.time;
    }

    void advanceSpawnIndex()
    {
        spawnIndex += 1;
        if (spawnIndex >= spawnPoints.Length)
        {
            spawnIndex = 0;
        }
    }
}
