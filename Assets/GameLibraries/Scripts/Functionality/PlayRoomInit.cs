﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayRoomInit : MonoBehaviour {
    public MidiEditor converter;
    public ReplayerGameFormat replayer;
    public bool debugMode = true;
    public bool unactive = false;
 
	// Use this for initialization
	void Start () {
        if (!debugMode)
            converter.ConvertMidiToGameFormat();
        
	}

    void Update()
    {
        if (!unactive)
        {
            if (converter.progress == 1f && replayer.progress == 0f)
            {
                replayer.init();
            }
            if (replayer.progress == 1f)
            {
                replayer.PlaySong();
                unactive = true;
            }
        }
        
    }
	
}
