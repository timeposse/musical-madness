﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.IO;
using System;
using System.ComponentModel;

[RequireComponent(typeof(LoadingBarController))]
public class MidiDownloader : MonoBehaviour 
{
    string directory;
    public float progress = 0f;
    public bool pollCrawler = true;
    WebCrawler crawler;

    LoadingBarController loadbar;

    void Start()
    {
        directory = Directory.GetCurrentDirectory() + "/DevTemp/";
        crawler = GetComponent<WebCrawler>();

        loadbar = GetComponent<LoadingBarController>();
        //string url = hostname + filename;
        //string destFile = "DevTemp/" + filename;
        //DownloadFile(url, destFile);
    }

    void Update()
    {
        if (!FoxHelper.debugMode)
        {
            if (loadbar != null)
                loadbar.SetProgress(progress);

        }
        
        if (pollCrawler || crawler != null)
        {
            string midiUrl = crawler.finalCrawledURL;
            if (midiUrl != "")
            {
                Debug.Log("Downloading from " + midiUrl);
                DownloadFile(midiUrl, "temp.mid");
                crawler.finalCrawledURL = "";
            }
        }
    }

    

    public void DownloadFile(string url, string filename)
    {
        FoxHelper.ClearTempSpace(directory);
        progress = 0;

        WebClient Client = new WebClient ();
        ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
        
        string saveLoc = directory + filename;
        Debug.Log("Save Location: " + saveLoc);

        Debug.Log("Download URL: " + url);

        //Client.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadFileCallback2);
        // Specify a progress notification handler.
        Client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(DownloadProgressCallback);

        //Client.DownloadFileAsync(url, @saveLoc);
        Client.DownloadFileAsync(new Uri(url), saveLoc);
        
    }

    private void DownloadProgressCallback(object sender, DownloadProgressChangedEventArgs e)
    {
        // Displays the operation identifier, and the transfer progress.
        progress = e.ProgressPercentage / 100f;
        Debug.Log((string)e.UserState + " downloaded " + e.BytesReceived + " of " + e.TotalBytesToReceive + " bytes. " + e.ProgressPercentage + " % complete...");
        if (e.ProgressPercentage == 100)
        {
            progress = 1f;
        }
    }
}
