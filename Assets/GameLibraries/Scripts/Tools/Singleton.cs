﻿using UnityEngine;
using System.Collections;

public class Singleton : MonoBehaviour {
    private static Singleton instance = null;

    // Game Instance Singleton
    protected Singleton() { }

    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

}
