﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class FoxHelper
{
    public static bool debugMode = false;

    public static void ClearTempSpace(string directory)
    {

        Debug.Log("Clear temp space: " + directory);
        System.IO.DirectoryInfo di = new DirectoryInfo(directory + "/");

        foreach (FileInfo file in di.GetFiles())
        {
            file.Delete();
        }
        foreach (DirectoryInfo dir in di.GetDirectories())
        {
            dir.Delete(true);
        }
    }

    public static Dictionary<char, char> shiftLookup = new Dictionary<char, char>
    {
        {'9', '('},
        {'8', '*'},
        {'7', '&'},
        {'6', '^'},
        {'5', '%'},
        {'4', '$'},
        {'3', '#'},
        {'2', '@'},
        {'1', '!'},
        {'0', ')'}
    };

}
