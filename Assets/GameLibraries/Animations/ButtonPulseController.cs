﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Animator))]
public class ButtonPulseController : MonoBehaviour {
    Animator anim;

    void Start() 
    {
        anim = GetComponent<Animator>();
        if (anim == null)
        {
            throw new Exception("ButtonPulseController requires an Animator");
        }
    }


    public void playHoverAnimation()
    {
        Debug.Log("Hover over Start");
        anim.SetBool("isHovered", true);
    }

    public void stopHoverAnimation()
    {
        Debug.Log("Hover out Start");
        anim.SetBool("isHovered", false);
    }
}
